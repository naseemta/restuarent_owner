import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:restaurent_app/src/restaurentapp.dart';

void main() {
  /*SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
    statusBarColor: Color(0xffab000d), //or set color with: Color(0xFF0000FF)
  ));*/
  /*SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.white, //top bar color
        statusBarIconBrightness: Brightness.dark, //top bar icons
       *//* systemNavigationBarColor: Colors.white, //bottom bar color
        systemNavigationBarIconBrightness: Brightness.dark,*//* //bottom bar icons
      )
  );*/

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new RestaurantAppWrapper());
  });
}

