// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ar locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'ar';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "attendance" : MessageLookupByLibrary.simpleMessage("الحضور"),
    "contacts" : MessageLookupByLibrary.simpleMessage("جهات الاتصال"),
    "language" : MessageLookupByLibrary.simpleMessage("English"),
    "links" : MessageLookupByLibrary.simpleMessage("الروابط"),
    "login" : MessageLookupByLibrary.simpleMessage("تسجيل الدخول"),
    "not_valid_username" : MessageLookupByLibrary.simpleMessage("اسم المستخدم غير صحيح"),
    "password" : MessageLookupByLibrary.simpleMessage("كلمة المرور"),
    "password_is_too_short" : MessageLookupByLibrary.simpleMessage("كلمة المرور غير صحيحه"),
    "profile" : MessageLookupByLibrary.simpleMessage("الملف الشخصي"),
    "support" : MessageLookupByLibrary.simpleMessage("الدعم"),
    "username" : MessageLookupByLibrary.simpleMessage("اسم المستخدم"),
    "member_registration" : MessageLookupByLibrary.simpleMessage("تسجيل عضو"),
    "forgot_password" : MessageLookupByLibrary.simpleMessage("هل نسيت كلمة المرور"),
    "not_valid_password" : MessageLookupByLibrary.simpleMessage("كلمة مرور غير صالحة"),
    "loading" : MessageLookupByLibrary.simpleMessage("...جار التحميل"),
    "forgot_password_caption" : MessageLookupByLibrary.simpleMessage(".الرجاء إدخال اسم المستخدم الخاص بك لطلب إعادة تعيين كلمة المرور"),
    "reset_password" : MessageLookupByLibrary.simpleMessage("إعادة تعيين كلمة المرور"),
    "hi" : MessageLookupByLibrary.simpleMessage("مرحبا"),
    "my_orders" : MessageLookupByLibrary.simpleMessage("طلباتي"),
    "log_out" : MessageLookupByLibrary.simpleMessage("الخروج"),
    "log_out_confirmation" : MessageLookupByLibrary.simpleMessage("هل أنت متأكد أنك تريد تسجيل الخروج؟"),
    "scan_qr_caption" : MessageLookupByLibrary.simpleMessage("مسح رمز الاستجابة السريعة على الجدول لبدء الطلب"),
    "ok" : MessageLookupByLibrary.simpleMessage("حسنا"),
    "cancel" : MessageLookupByLibrary.simpleMessage("إلغاء"),
    "scan_qr" : MessageLookupByLibrary.simpleMessage("ممسح QR"),
    "invalid_qr" : MessageLookupByLibrary.simpleMessage("QR غير صالح"),
    "item_not_available" : MessageLookupByLibrary.simpleMessage("غير متوفر"),
    "add_items" : MessageLookupByLibrary.simpleMessage("الرجاء إضافة عناصر إلى سلة التسوق الخاصة بك"),
    "order_summary" : MessageLookupByLibrary.simpleMessage("ملخص الطلب"),
    "dishes" : MessageLookupByLibrary.simpleMessage("أطباق"),
    "items" : MessageLookupByLibrary.simpleMessage("العناصر"),
    "total_amount" : MessageLookupByLibrary.simpleMessage("المبلغ الإجمالي"),
    "place_order" : MessageLookupByLibrary.simpleMessage("مكان الامر"),
    "order_placed" : MessageLookupByLibrary.simpleMessage("تم الطلب"),
     "order_placed_on" : MessageLookupByLibrary.simpleMessage("تم الطلب"),
    "order_placed_caption" : MessageLookupByLibrary.simpleMessage("تم تقديم طلبك بنجاح"),
    "order_no" : MessageLookupByLibrary.simpleMessage("رقم الطلب"),
    "order_status" : MessageLookupByLibrary.simpleMessage("حالة الطلب"),
    "orders" : MessageLookupByLibrary.simpleMessage("أوامر"),
    "products" : MessageLookupByLibrary.simpleMessage("منتجات"),
    "calories" : MessageLookupByLibrary.simpleMessage("سعر كالوري"),
    "cust_name" : MessageLookupByLibrary.simpleMessage("اسم الزبون"),
    "phone_number" : MessageLookupByLibrary.simpleMessage("رقم الهاتف"),
    "update_status" : MessageLookupByLibrary.simpleMessage("تحديث حالة الطلب"),
    "update_status_confirmation" : MessageLookupByLibrary.simpleMessage("هل تريد بالتأكيد تحديث الحالة إلى"),

  };
}
