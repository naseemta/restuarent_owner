// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "attendance" : MessageLookupByLibrary.simpleMessage("Attendance"),
    "contacts" : MessageLookupByLibrary.simpleMessage("Contacts"),
    "language" : MessageLookupByLibrary.simpleMessage("عربي"),
    "links" : MessageLookupByLibrary.simpleMessage("Links"),
    "login" : MessageLookupByLibrary.simpleMessage("Login"),
    "not_valid_username" : MessageLookupByLibrary.simpleMessage("Not Valid Username"),
    "password" : MessageLookupByLibrary.simpleMessage("password"),
    "password_is_too_short" : MessageLookupByLibrary.simpleMessage("password is too short"),
    "profile" : MessageLookupByLibrary.simpleMessage("Profile"),
    "support" : MessageLookupByLibrary.simpleMessage("Support"),
    "username" : MessageLookupByLibrary.simpleMessage("Username"),
    "member_registration" : MessageLookupByLibrary.simpleMessage("Member Registration"),
    "forgot_password" : MessageLookupByLibrary.simpleMessage("Forgot Password"),
    "not_valid_password" : MessageLookupByLibrary.simpleMessage("Not Valid Password"),
    "loading" : MessageLookupByLibrary.simpleMessage("Loading..."),
    "forgot_password_caption" : MessageLookupByLibrary.simpleMessage("Please enter your username to request a password reset."),
    "reset_password" : MessageLookupByLibrary.simpleMessage("Reset Password"),
    "hi" : MessageLookupByLibrary.simpleMessage("Hi"),
    "my_orders" : MessageLookupByLibrary.simpleMessage("Open Orders"),
    "log_out" : MessageLookupByLibrary.simpleMessage("Log Out"),
    "log_out_confirmation" : MessageLookupByLibrary.simpleMessage("Are you sure you want to log out?"),
    "scan_qr_caption" : MessageLookupByLibrary.simpleMessage("Scan the QR code on the table to start ordering"),
    "ok" : MessageLookupByLibrary.simpleMessage("OK"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "scan_qr" : MessageLookupByLibrary.simpleMessage("Scan QR"),
    "invalid_qr" : MessageLookupByLibrary.simpleMessage("Invalid QR"),
    "item_not_available" : MessageLookupByLibrary.simpleMessage("Not available"),
    "add_items" : MessageLookupByLibrary.simpleMessage("Please add items to your cart"),
    "order_summary" : MessageLookupByLibrary.simpleMessage("Order Summary"),
    "dishes" : MessageLookupByLibrary.simpleMessage("Dishes"),
    "items" : MessageLookupByLibrary.simpleMessage("Items"),
    "total_amount" : MessageLookupByLibrary.simpleMessage("Total Amount"),
    "place_order" : MessageLookupByLibrary.simpleMessage("Place Order"),
    "order_placed" : MessageLookupByLibrary.simpleMessage("Order placed"),
     "order_placed_on" : MessageLookupByLibrary.simpleMessage("Order Placed On"),
    "order_placed_caption" : MessageLookupByLibrary.simpleMessage("Your order has been placed successfully"),
    "order_no" : MessageLookupByLibrary.simpleMessage("Order Number"),
    "order_status" : MessageLookupByLibrary.simpleMessage("Order Status"),
    "orders" : MessageLookupByLibrary.simpleMessage("Orders"),
    "products" : MessageLookupByLibrary.simpleMessage("Products"),
    "calories" : MessageLookupByLibrary.simpleMessage("calories"),
    "cust_name" : MessageLookupByLibrary.simpleMessage("Customer Name"),
    "phone_number" : MessageLookupByLibrary.simpleMessage("Phone Number"),
    "update_status" : MessageLookupByLibrary.simpleMessage("Update Order Status"),
    "update_status_confirmation" : MessageLookupByLibrary.simpleMessage("Are you sure you want to update status to "),

  };
}
