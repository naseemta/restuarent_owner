import 'package:flutter/material.dart';

Map<int, Color> color =
{
  50:Color.fromRGBO(248,37,74, .1),
  100:Color.fromRGBO(248,37,74, .2),
  200:Color.fromRGBO(248,37,74, .3),
  300:Color.fromRGBO(248,37,74, .4),
  400:Color.fromRGBO(248,37,74, .5),
  500:Color.fromRGBO(248,37,74, .6),
  600:Color.fromRGBO(248,37,74, .7),
  700:Color.fromRGBO(248,37,74, .8),
  800:Color.fromRGBO(248,37,74, .9),
  900:Color.fromRGBO(248,37,74, 1),
};

MaterialColor colorRestaurant = MaterialColor(0xFFF8254A, color);
