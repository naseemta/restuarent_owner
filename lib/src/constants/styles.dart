
import 'package:flutter/material.dart';

 TextStyle buttonTextStyle= new TextStyle(color: Colors.white,
  fontSize: 20.0,
  fontWeight: FontWeight.w300,
  letterSpacing: 0.3,
);

TextStyle buttonCaptionStyle= new TextStyle(
    color:  Colors.white,
    fontWeight: FontWeight.w400,
    fontFamily: "Roboto",
    fontStyle:  FontStyle.normal,
    fontSize: 18.0,
);

TextStyle titleStyle= new TextStyle(
    color:  Colors.white,
    fontWeight: FontWeight.w500,
    fontFamily: "Roboto",
    fontStyle:  FontStyle.normal,
    fontSize: 18.0
);


TextStyle myOrdersStyle= new TextStyle(
    color:   Colors.white,
    fontWeight: FontWeight.w400,
    fontFamily: "Roboto",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
);


TextStyle itemTitleStyle= new TextStyle(
    color:  const Color(0xff363a3d),
    fontWeight: FontWeight.w500,
    fontFamily: "Roboto",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
);

TextStyle itemPriceStyle= new TextStyle(
    color:  const Color(0xff363a3d),
    fontWeight: FontWeight.w500,
    fontFamily: "Roboto",
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
);

TextStyle checkoutTitleStyle= new TextStyle(
    color:  const Color(0xffffffff),
    fontWeight: FontWeight.w500,
    fontFamily: "Roboto",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
);

TextStyle itemDescriptionStyle= new TextStyle(
    color:  const Color(0xff60666a),
    fontWeight: FontWeight.w300,
    fontFamily: "Roboto",
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
);

TextStyle itemDescriptionStyleError= new TextStyle(
    color:  const Color(0xffff0000),
    fontWeight: FontWeight.w300,
    fontFamily: "Roboto",
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
);

TextStyle itemTotalStyle= new TextStyle(
color:  const Color(0xff0b2031),
fontWeight: FontWeight.w500,
fontFamily: "Roboto",
fontStyle:  FontStyle.normal,
fontSize: 18.0
);

TextStyle itemCheckoutPriceStyle= new TextStyle(
color:  const Color(0xff20ab2c),
fontWeight: FontWeight.w400,
fontFamily: "Roboto",
fontStyle:  FontStyle.normal,
fontSize: 16.0
);

TextStyle signUpTitleStyle= new TextStyle(
    color:  const Color(0xFFFFFFFF),
    fontWeight: FontWeight.w500,
    fontFamily: "Roboto",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0,
 decoration: TextDecoration.underline
);

TextStyle signUpTitleNormalStyle= new TextStyle(
    color:  const Color(0xFFFFFFFF),
    fontWeight: FontWeight.w500,
    fontFamily: "Roboto",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0,
);
