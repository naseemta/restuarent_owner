

import 'package:flutter/material.dart';
import 'package:restaurent_app/src/constants/colors.dart';

class Utils
{
static showSnackBar(GlobalKey<ScaffoldState> scaffoldKey, String message) {
  scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        backgroundColor: colorRestaurant,
        content: new Text(message ?? 'You are offline'),
      )
  );
}
}