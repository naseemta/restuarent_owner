import 'package:restaurent_app/src/models/authmodel.dart';
import 'package:restaurent_app/src/models/backgroundimageresponse.dart';
import 'package:shared_preferences/shared_preferences.dart';
class AuthUtils {

  static final String endPoint = 'api/oauth/token';
  // Keys to store and fetch data from SharedPreferences
  static final String authTokenKey = 'access_token';
  static final String userIdKey = 'user_id';
  static final String nameKey = 'userName';
  static final String refreshTokenKey = 'refresh_token';
  static final String homeBg = 'home_bg';
  static final String loginBg = 'login_bg';
  static final String languageKey= 'lang';

  static String getToken(SharedPreferences prefs) {
    return prefs.getString(authTokenKey);
  }

  static String getRefreshToken(SharedPreferences prefs) {
    return prefs.getString(refreshTokenKey);
  }

  static String getUserName(SharedPreferences prefs) {
    return prefs.getString(nameKey);
  }

  static String getHomeBG(SharedPreferences prefs) {
    return prefs.getString(homeBg);
  }

  static String getLoginBG(SharedPreferences prefs) {
    return prefs.getString(loginBg);
  }

  static insertDetails(SharedPreferences prefs, AuthResponse response) {
    prefs.setString(authTokenKey, response.accessToken);
    prefs.setString(refreshTokenKey, response.refreshToken);
    prefs.setString(nameKey, response.userName);
  }

  static insertImageDetails(SharedPreferences prefs, BackgroundImageResponse response) {
    prefs.setString(homeBg, response.home);
    prefs.setString(loginBg, response.login);
  }

  static saveLanguage(SharedPreferences prefs,String language) {
    prefs.setString(languageKey, language);
  }

  static String getLanguage(SharedPreferences prefs) {
    return prefs.getString(languageKey);
  }

  static deleteDetails(SharedPreferences prefs) {
    prefs.setString(authTokenKey,null);
    prefs.setString(refreshTokenKey, null);
    prefs.setString(nameKey, null);
  }


}