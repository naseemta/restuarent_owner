class AuthResponse {
  String accessToken;
  String userName;
  String refreshToken;
  bool isSuccess;
  String error;
  String errorDescription;

  AuthResponse({this.isSuccess, this.accessToken, this.userName, this.refreshToken,this.error, this.errorDescription});

  factory AuthResponse.fromJson(
      Map<String, dynamic> parsedJson, bool isSuccess) {
    if (isSuccess) {
      return AuthResponse(
        isSuccess: true
        ,accessToken: parsedJson['access_token'],
        userName: parsedJson['MemberName'],
        refreshToken: parsedJson['refresh_token'],
      );
    } else
      return AuthResponse(
        isSuccess: false,
        error: parsedJson['error'],
        errorDescription: parsedJson['error_description'],
      );
  }
}
