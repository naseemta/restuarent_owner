//[{"success":"true","message":"Order Pending","order_no":"1000000002"}]
class UpdateItemAvailabilityResponse {
  int statusCode;
  bool isSuccess;
  String error;
  String errorDescription;
  static final String endPoint = 'api/menu/upavail';
  List<StatusItem> statusList;
  String message;
  String next;


  UpdateItemAvailabilityResponse({ this.statusList,this.next,this.statusCode,this. isSuccess});

  factory UpdateItemAvailabilityResponse.fromJson(
      List<dynamic> parsedJson,int statusCode,bool isSuccess) {

      if(isSuccess) {
        var list = parsedJson as List;
        List<StatusItem> orderList =
        list.map((i) => StatusItem.fromJson(i)).toList();

        return UpdateItemAvailabilityResponse(
            statusList: orderList,
            statusCode: statusCode,
            isSuccess: isSuccess);
      }
      else
        {
          return UpdateItemAvailabilityResponse(
              statusCode: statusCode,
              isSuccess: isSuccess);
        }
  }
  factory UpdateItemAvailabilityResponse.fromJsonDecodeError(
      Map<String, dynamic> parsedJson,int statusCode,bool isSuccess) {

    if(isSuccess) {
      var list = parsedJson as List;
      List<StatusItem> orderList =
      list.map((i) => StatusItem.fromJson(i)).toList();

      return UpdateItemAvailabilityResponse(
          statusList: orderList,
          statusCode: statusCode,
          isSuccess: isSuccess);
    }
    else
    {
      return UpdateItemAvailabilityResponse(
          statusCode: statusCode,
          isSuccess: isSuccess);
    }
  }

}



class StatusItem {
  int statusId;
  String statusName;


  StatusItem({
    this.statusId,
    this.statusName,
   });

  factory StatusItem.fromJson(Map<String, dynamic> parsedJson) {
    return StatusItem(
      statusId: parsedJson['order_status_id'],
      statusName: parsedJson['order_status'],
     );
  }

}
