class NetworkError
{
  String error;
  String errorDescription;

  NetworkError({this.error,this.errorDescription});

  factory NetworkError.fromJson(Map<String, dynamic> parsedJson){

    return NetworkError(
      error: parsedJson['error'],
      errorDescription:parsedJson['error_description'],
    );

  }
}