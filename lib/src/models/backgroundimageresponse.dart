
class BackgroundImageResponse
{
  String splash,home,login;
  BackgroundImageResponse({this.splash,this.home,this.login});
  factory BackgroundImageResponse.fromJson(Map<String, dynamic> parsedJson) {
      return BackgroundImageResponse(
          splash: parsedJson['splash'],
          home:  parsedJson['home'], login:  parsedJson['login']);
    }
}
