class ForgotPasswordResponse {

  String message;
  String status;
  static final String endPoint = 'api/PasswordChange/getid';

  ForgotPasswordResponse({this.status, this.message});
  //[{"Message":"Your Password has been changed successfully.","Status":"1"}]
  factory ForgotPasswordResponse.fromJson(
      List< dynamic> parsedJson) {
      return ForgotPasswordResponse(
        message: parsedJson[0]['Message'],
        status: parsedJson[0]['Status'],
      );
  }
}
