//[{"success":"true","message":"Order Pending","order_no":"1000000002"}]
class OrderResponse {
  String isSuccess;
  int statusCode;
  String error;
  String errorDescription;
  static final String endPoint = 'api/orders/getOrder';
  static final String endPointStatus = 'api/restorders/updateStatus';
  static final String updateAvailability = 'api/menu/upavail';

  String message;
  String orderNo;


  OrderResponse(
      {this.isSuccess, this.error,  this.orderNo,this.message});

  factory OrderResponse.fromJson(
      List<dynamic> parsedJson) {

    return OrderResponse(
        isSuccess: parsedJson[0]['success'],
        orderNo: parsedJson[0]['order_no'],
        message: parsedJson[0]['message']);
  }
}
