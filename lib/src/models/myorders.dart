//[{"success":"true","message":"Order Pending","order_no":"1000000002"}]
class MyOrderResponse {
  int statusCode;
  bool isSuccess;
  String error;
  String errorDescription;
  static final String endPoint = 'api/restorders/';
  List<MenuItem> orderList;
  String message;
  String next;


  MyOrderResponse({ this.orderList,this.next,this.statusCode,this. isSuccess});

  factory MyOrderResponse.fromJson(
      List<dynamic> parsedJson,int statusCode,bool isSuccess) {

      if(isSuccess) {
        var list = parsedJson[0]['order_list'] as List;
        List<MenuItem> orderList =
        list.map((i) => MenuItem.fromJson(i)).toList();

        return MyOrderResponse(
            orderList: orderList,
            next: parsedJson[0]['next'],
            statusCode: statusCode,
            isSuccess: isSuccess);
      }
      else
        {
          return MyOrderResponse(
              statusCode: statusCode,
              isSuccess: isSuccess);
        }
  }
  factory MyOrderResponse.fromJsonDecodeError(
      Map<String, dynamic> parsedJson,int statusCode,bool isSuccess) {

    if(isSuccess) {
      var list = parsedJson[0]['order_list'] as List;
      List<MenuItem> orderList =
      list.map((i) => MenuItem.fromJson(i)).toList();

      return MyOrderResponse(
          orderList: orderList,
          next: parsedJson[0]['next'],
          statusCode: statusCode,
          isSuccess: isSuccess);
    }
    else
    {
      return MyOrderResponse(
          statusCode: statusCode,
          isSuccess: isSuccess);
    }
  }

}

class MenuItem {
  String orderNO;
  String orderStatus;
  int orderStatusId;
  String restaurantId;
  String restaurantName;
  String restaurantImage;
  String tableId;
  String tableName;
  String branchName;
  String branchID;
  String memberName;
  String memberPhone;
  DateTime orderDate;
  double grandTotal;
  List<DishItem> menuCategoryList;

  MenuItem(
      {this.orderNO,
        this.orderStatus,
        this.orderStatusId,
        this.restaurantId,
        this.restaurantName,
        this.restaurantImage,
        this.tableId,
        this.tableName,
        this.branchName,
        this.branchID,
        this.memberName,
        this.memberPhone,
        this.orderDate,
        this.grandTotal,
        this.menuCategoryList});

  factory MenuItem.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['category_dishes'] as List;
    List<DishItem> dataList =
    list.map((i) => DishItem.fromJson(i)).toList();

    return MenuItem(
        orderNO: parsedJson['order_no'],
        orderStatus: parsedJson['order_status'],
        orderStatusId:  parsedJson['order_status_id'],
        restaurantId: parsedJson['restaurant_id'],
        restaurantName: parsedJson['restaurant_name'],
        restaurantImage: parsedJson['restaurant_image'],
        tableId: parsedJson['table_id'],
        orderDate: DateTime.parse(parsedJson['order_date']),
        memberName: parsedJson['Member_name'],
        memberPhone: parsedJson['phone_no'],
        tableName: parsedJson['table_name'],
        branchName: parsedJson['branch_name'],
        branchID: parsedJson['branch_id'],
        grandTotal: parsedJson['grandtotal'],
        menuCategoryList: dataList);
  }
}


class DishItem {
  String dishId;
  String dishName;
  double dishPrice;
  String dishImage;
  String dishCurrency;
  double dishCalories;
  String dishDescription;
  bool dishAvailability;
  int dishType;
  double count=0;
  List<DishAddonItem> dishAddOnList;

  DishItem({
    this.dishId,
    this.dishName,
    this.dishPrice,
    this.dishImage,
    this.dishCurrency,
    this.dishCalories,
    this.dishType,
    this.dishDescription,
    this.dishAvailability,
    this.count,
    this.dishAddOnList});

  factory DishItem.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['dishAddon_list'] as List;
    List<DishAddonItem> dataList =
    list.map((i) => DishAddonItem.fromJson(i)).toList();
    return DishItem(
        dishId: parsedJson['dish_id'],
        dishName: parsedJson['dish_name'],
        dishPrice: parsedJson['dish_price'],
        dishType: parsedJson['dish_Type'],
        count: parsedJson['dish_quantity'],
        dishAddOnList: dataList
    );
  }

  Map<String, dynamic> toJsonData() {
    var map = new Map<String, dynamic>();
    map["dish_id"] = this.dishId;
    map["dish_name"] = this.dishName;
    map["dish_price"] = this.dishPrice;
    map["dish_quantity"] = this.count;
    return map;
  }
}
class DishAddonItem {
  String dishAddonName;
  int dishCategoryType;
  DishAddonItem({this.dishAddonName,this.dishCategoryType});

  factory DishAddonItem.fromJson(Map<String, dynamic> parsedJson) {

    return DishAddonItem(
      dishAddonName: parsedJson['addon_name'],
      dishCategoryType:parsedJson['addon_Cat_Type'],
    );
  }

}
