class MenuItemResponse {
  bool isSuccess;
  int statusCode;
  String error;
  String errorDescription;
  static final String endPoint = 'api/menu/getdata';
  static final String endPointOwnerMenu = 'api/menu/RestMenu';

  String accessToken;
  String userName;
  String refreshToken;
  MenuResponseItem restaurantItem;

  MenuItemResponse(
      {this.isSuccess, this.error, this.restaurantItem, this.statusCode});

  factory MenuItemResponse.fromJson(
      List<dynamic> parsedJson, bool isSuccess, int statusCode) {
    if (isSuccess) {

      MenuResponseItem restaurantItem = MenuResponseItem.fromJson(parsedJson[0]);
      return MenuItemResponse(
          isSuccess: true,
          restaurantItem: restaurantItem,
          statusCode: statusCode);
    } else {
      print("else"+statusCode.toString());
      return MenuItemResponse(
          isSuccess: false,
          statusCode: statusCode);
    }
  }
}

class MenuResponseItem {
  String restaurantId;
  String restaurantName;
  String restaurantImage;
  String tableId;
  String tableName;
  String branchName;
  String nexturl;
  List<MenuCategoryItem> menuCategoryList;

  MenuResponseItem(
      {this.restaurantId,
      this.restaurantName,
      this.restaurantImage,
      this.tableId,
      this.tableName,
      this.branchName,
      this.nexturl,
      this.menuCategoryList});

  factory MenuResponseItem.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['table_menu_list'] as List;
    List<MenuCategoryItem> dataList =
        list.map((i) => MenuCategoryItem.fromJson(i)).toList();

    return MenuResponseItem(
        restaurantId: parsedJson['restaurant_id'],
        restaurantName: parsedJson['restaurant_name'],
        restaurantImage: parsedJson['restaurant_image'],
        tableId: parsedJson['table_id'],
        tableName: parsedJson['table_name'],
        branchName: parsedJson['branch_name'],
        nexturl: parsedJson['nexturl'],
        menuCategoryList: dataList);
  }
}

class MenuCategoryItem {
  String menuCategory;
  String menuCategoryId;
  String menuCategoryImage;
  String nexturl;
  List<MenuCategoryDishItem> menuCategoryDishList;

  MenuCategoryItem(
      {this.menuCategory,
      this.menuCategoryId,
      this.menuCategoryImage,
      this.nexturl,
      this.menuCategoryDishList});

  factory MenuCategoryItem.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['category_dishes'] as List;
    List<MenuCategoryDishItem> dataList =
        list.map((i) => MenuCategoryDishItem.fromJson(i)).toList();

    return MenuCategoryItem(
        menuCategory: parsedJson['menu_category'],
        menuCategoryId: parsedJson['menu_category_id'],
        menuCategoryImage: parsedJson['menu_category_image'],
        nexturl: parsedJson['nexturl'],
        menuCategoryDishList: dataList);
  }
}

class MenuCategoryDishItem {
  String dishId;
  String dishName;
  double dishPrice;
  String dishImage;
  String dishCurrency;
  double dishCalories;
  String dishDescription;
  bool dishAvailability;
  int dishType;
  int count=0;

  MenuCategoryDishItem({this.dishId,
      this.dishName,
      this.dishPrice,
      this.dishImage,
      this.dishCurrency,
      this.dishCalories,
      this.dishType,
      this.dishDescription,
      this.dishAvailability});

  factory MenuCategoryDishItem.fromJson(Map<String, dynamic> parsedJson) {
    return MenuCategoryDishItem(
        dishId: parsedJson['dish_id'],
        dishName: parsedJson['dish_name'],
        dishPrice: parsedJson['dish_price'],
        dishImage: parsedJson['dish_image'],
        dishType: parsedJson['dish_Type'],
        dishCurrency: parsedJson['dish_currency'],
        dishCalories: parsedJson['dish_calories'],
        dishDescription: parsedJson['dish_description'],
        dishAvailability: parsedJson['dish_Availability']);
  }

  Map<String, dynamic> toJsonData() {
    var map = new Map<String, dynamic>();
    map["dish_id"] = this.dishId;
    map["dish_name"] = this.dishName;
    map["dish_price"] = this.dishPrice;
    map["dish_quantity"] = this.count;
    return map;
  }
}
