import 'package:flutter/material.dart';
import 'package:restaurent_app/src/constants/colors.dart';
import 'package:restaurent_app/src/models/auth_utils.dart';
import 'package:restaurent_app/src/translation.dart';
import 'package:restaurent_app/src/ui/login.dart';
import 'package:restaurent_app/src/ui/owner_home.dart';
import 'package:restaurent_app/src/ui/scanhome.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RestaurantAppWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModel<AppModel>(model: AppModel(), child: RestaurantApp());
  }
}

class AppModel extends Model {
  Locale _appLocale = Locale('en');
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences _sharedPreferences;
  Locale get appLocal => _appLocale ?? Locale("en");

  void changeDirection() {
    if (_appLocale == Locale("ar")) {
      _appLocale = Locale("en");
      _saveLanguage("en");

    } else {
      _appLocale = Locale("ar");
      _saveLanguage("ar");

    }
    notifyListeners();
  }

  _saveLanguage(String language) async {
    _sharedPreferences = await _prefs;
    AuthUtils.saveLanguage(_sharedPreferences, language);
  }


  void changeDirectionLang(String lang) {
      _appLocale = Locale(lang);
    notifyListeners();
  }
}

class RestaurantApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppModel>(
        builder: (context, child, model) => MaterialApp(
              locale: model.appLocal,
              localizationsDelegates: [
                const TranslationsDelegate(),
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              supportedLocales: [
                const Locale('ar', ''), // Arabic
                const Locale('en', ''), // English
              ],

              theme: ThemeData(primarySwatch: colorRestaurant /*,canvasColor: colorRestaurant*/),
              debugShowCheckedModeBanner: false,
              home: new LoginPage(),
              routes: {
                HomePage.routeName: (BuildContext context) => new HomePage(),
                OwnerHomePage.routeName: (BuildContext context) => new OwnerHomePage(),
              },
            ));
  }
}
