import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:restaurent_app/src/l10n/messages_all.dart';

class Translations {


  static Future<Translations> load(Locale locale) {
    final String name =
        (locale.countryCode != null && locale.countryCode.isEmpty)
            ? locale.languageCode
            : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((dynamic _) {
      Intl.defaultLocale = localeName;
      return new Translations();
    });
  }

  static Translations of(BuildContext context) {
    return Localizations.of<Translations>(context, Translations);
  }

  String get username {
    return Intl.message(
      'Username',
      name: 'username',
    );
  }

  String get not_valid_username {
    return Intl.message(
      'Not Valid Username',
      name: 'not_valid_username',
    );
  }

  String get password {
    return Intl.message(
      'password',
      name: 'password',
    );
  }

  String get password_is_too_short {
    return Intl.message(
      'password is too short',
      name: 'password_is_too_short',
    );
  }

  String get login {
    return Intl.message(
      'Login',
      name: 'login',
    );
  }

  String get language {
    return Intl.message(
      'عربي',
      name: 'language',
    );
  }

  String get links {
    return Intl.message(
      'Links',
      name: 'links',
    );
  }

  String get contacts {
    return Intl.message(
      'Contacts',
      name: 'contacts',
    );
  }

  String get attendance {
    return Intl.message(
      'Attendance',
      name: 'attendance',
    );
  }

  String get support {
    return Intl.message(
      'Support',
      name: 'support',
    );
  }

  String get profile {
    return Intl.message(
      'Profile',
      name: 'profile',
    );
  }

  String get member_registration {
    return Intl.message(
      'Member Registration',
      name: 'member_registration',
    );
  }

  String get not_valid_password {
    return Intl.message(
      'Not Valid Password',
      name: 'not_valid_password',
    );
  }
  String get forgot_password {
    return Intl.message(
      'Forgot Password',
      name: 'forgot_password',
    );
  }

  String get forgot_password_caption {
    return Intl.message(
      'Please enter your username to request a password reset.',
      name: 'forgot_password_caption',
    );
  }
  String get reset {
    return Intl.message(
      'Please enter your username to request a password reset.',
      name: 'forgot_password_caption',
    );
  }
   String get loading {
    return Intl.message(
      'Loading...',
      name: 'loading',
    );
  }
 String get reset_password {
    return Intl.message(
      'Reset Password',
      name: 'reset_password',
    );
  }
  String get hi {
    return Intl.message(
      'Hi',
      name: 'hi',
    );
  }
   String get my_orders {
    return Intl.message(
      'Open Orders',
      name: 'my_orders',
    );
  }
  String get log_out {
    return Intl.message(
      'Log Out',
      name: 'log_out',
    );
  }
  String get log_out_confirmation {
    return Intl.message(
      'Are you sure you want to log out?',
      name: 'log_out_confirmation',
    );
  }
    String get scan_qr_caption {
    return Intl.message(
      'Scan the QR code on the table to start ordering',
      name: 'scan_qr_caption',
    );
  }
   String get ok {
    return Intl.message(
      'OK',
      name: 'ok',
    );
  }
   String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
    );
  }
   String get scan_qr {
    return Intl.message(
      'Scan QR',
      name: 'scan_qr',
    );
  }
  String get invalid_qr {
    return Intl.message(
      'Invalid QR',
      name: 'invalid_qr',
    );
  }
    String get item_not_available {
    return Intl.message(
      'Item not available',
      name: 'item_not_available',
    );
  }
   String get add_items {
    return Intl.message(
      'Please add items to your cart',
      name: 'add_items',
    );
  }

  String get order_summary {
    return Intl.message(
      'Order Summary',
      name: 'order_summary',
    );
  }
   
   String get dishes {
    return Intl.message(
      'Dishes',
      name: 'dishes',
    );
  }

  String get order_status {
    return Intl.message(
      'Order Status',
      name: 'order_status',
    );
  }

 String get items {
    return Intl.message(
      'Items',
      name: 'items',
    );
  }
   String get total_amount {
    return Intl.message(
      'Total Amount',
      name: 'total_amount',
    );
  }

  String get place_order {
    return Intl.message(
      'Place Order',
      name: 'place_order',
    );
  }

  String get order_placed {
    return Intl.message(
      'Order placed',
      name: 'order_placed',
    );
  }
   String get order_placed_on {
    return Intl.message(
      'Order Placed On',
      name: 'order_placed_on',
    );
  }

  String get customer_name {
    return Intl.message(
      'Customer Name',
      name: 'customer_name',
    );
  }

   String get order_placed_caption {
    return Intl.message(
      'Your order has been placed successfully',
      name: 'order_placed_caption',
    );
  }
  String get order_no {
    return Intl.message(
      'Order Number',
      name: 'order_no',
    );
  }
  String get orders {
    return Intl.message(
      'Orders',
      name: 'orders',
    );
  }
  String get products {
    return Intl.message(
      'Products',
      name: 'products',
    );
  }
  String get calories {
    return Intl.message(
      'calories',
      name: 'calories',
    );
  }
  String get cust_name {
    return Intl.message(
      'Customer Name',
      name: 'cust_name',
    );
  }
  String get phone_number {
    return Intl.message(
      'Phone Number',
      name: 'phone_number',
    );
  }
  String get update_status {
    return Intl.message(
      'Update Order Status',
      name: 'update_status',
    );
  }
  String get update_status_confirmation {
    return Intl.message(
      'Are you sure you want to update status to ',
      name: 'update_status_confirmation',
    );
  }
}
