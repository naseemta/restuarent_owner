import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:restaurent_app/src/models/auth_utils.dart';
import 'package:restaurent_app/src/models/authmodel.dart';
import 'package:restaurent_app/src/models/backgroundimageresponse.dart';
import 'package:restaurent_app/src/models/forgotpasswordmodel.dart';
import 'package:restaurent_app/src/models/menu_model.dart';
import 'package:restaurent_app/src/models/myorders.dart';
import 'package:restaurent_app/src/models/orderdata.dart';
import 'package:restaurent_app/src/models/orderstatus.dart';
import 'package:restaurent_app/src/models/updateitem.dart';


class RestaurantAPIService {
  static String API_SERVER_URL = "http://api.unicomerp.net/";

  @override
  Future<AuthResponse> login(String username, String password) async {
    final url = AuthUtils.endPoint;
    print("uname-"+username);
    print("pwd-"+password);

    Map<String, String> body = {
      'grant_type': 'password',
      'UserName': username,
      'Password': password,
      'client_id': 'UniResApp',
    };
    try {

      print("url-"+API_SERVER_URL + url);
      final response = await http.post(Uri.parse(API_SERVER_URL + url),
         body: body, headers: {'Content-type': 'application/x-www-form-urlencoded'});
      print(response.body);
      print('status'+response.statusCode.toString());

      if (response.statusCode == 200) {
        print(response.body);
        return AuthResponse.fromJson(json.decode(response.body),true);
      } else if (response.statusCode >= 400) {
        print(response.body);
        return AuthResponse.fromJson(json.decode(response.body),false);
      }
      else {
        log("Failed http call."); // Perhaps handle it somehow
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }

  @override
  Future<AuthResponse> refresh(String refreshToken) async {
    final url = AuthUtils.endPoint;

    Map<String, String> body = {
      'grant_type': 'refresh_token',
      'refresh_token': refreshToken,
      'client_id': 'UniResApp',
    };
    try {

      print("url-"+API_SERVER_URL + url);
      final response = await http.post(Uri.parse(API_SERVER_URL + url),
          body: body, headers: {'Content-type': 'application/x-www-form-urlencoded'});
      print(response.body);
      print('status'+response.statusCode.toString());

      if (response.statusCode == 200) {
        print(response.body);
        return AuthResponse.fromJson(json.decode(response.body),true);
      } else if (response.statusCode >= 400) {
        print(response.body);
        return AuthResponse.fromJson(json.decode(response.body),false);
      }
      else {
        log("Failed http call."); // Perhaps handle it somehow
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }




  @override
  Future<MenuItemResponse> getMenu(String tableID,String restaurantID,String branchID,String languageID,String accessToken) async {
    final url = MenuItemResponse.endPoint;

    Map<String, String> body = {
      'table_id': tableID,
      'restaurant_id': restaurantID,
      'branch_id': branchID,
      'lang': languageID,
    };
    try {

      print("url-"+API_SERVER_URL + url);
      print("accessToken-"+accessToken);

     /* final response = await http.post(Uri.parse(API_SERVER_URL + url),

          body: body, headers: {'Content-Type':'application/x-www-form-urlencoded',
            'content-type': 'multipart/form-data','Authorization':"Bearer "+accessToken});
      print(response.body);
      print('status'+response.statusCode.toString());*/

      Map<String, String> headers = { 'Authorization': 'bearer '+accessToken, 'Content-Type': 'multipart/form-data'};
      final multipartRequest = new http.MultipartRequest('POST', Uri.parse((API_SERVER_URL + url)));
      multipartRequest.headers.addAll(headers);
      multipartRequest.fields.addAll(body);

      StreamedResponse postresponse = await multipartRequest.send();
      postresponse.stream.transform(utf8.decoder).listen((value) {
        print(value);
        print('response='+postresponse.statusCode.toString());
        if (postresponse.statusCode == 200) {
          print(value);
          return MenuItemResponse.fromJson(json.decode(value),true,postresponse.statusCode);
        } else if (postresponse.statusCode == 401) {
          print(value);
          return MenuItemResponse.fromJson(json.decode(value),false,postresponse.statusCode);
        }
        else {
          log("Failed http call."); // Perhaps handle it somehow
          return MenuItemResponse.fromJson(
              json.decode(value), false, postresponse.statusCode);
        }
      });

    } catch (exception) {
      log(exception.toString());
    }

    return null;
  }


  @override
  Future<MenuItemResponse> getOwnerMenu(String languageID,String accessToken) async {
    final url = MenuItemResponse.endPointOwnerMenu;

    Map<String, String> body = {
      'lang': languageID,
    };
    try {

      print("url-"+API_SERVER_URL + url);
      print("accessToken-"+accessToken);

      /* final response = await http.post(Uri.parse(API_SERVER_URL + url),

          body: body, headers: {'Content-Type':'application/x-www-form-urlencoded',
            'content-type': 'multipart/form-data','Authorization':"Bearer "+accessToken});
      print(response.body);
      print('status'+response.statusCode.toString());*/

      Map<String, String> headers = { 'Authorization': 'bearer '+accessToken, 'Content-Type': 'multipart/form-data'};
      final multipartRequest = new http.MultipartRequest('POST', Uri.parse((API_SERVER_URL + url)));
      multipartRequest.headers.addAll(headers);
      multipartRequest.fields.addAll(body);

      StreamedResponse postresponse = await multipartRequest.send();
      postresponse.stream.transform(utf8.decoder).listen((value) {
        print(value);
        print('response='+postresponse.statusCode.toString());
        if (postresponse.statusCode == 200) {
          print(value);
          return MenuItemResponse.fromJson(json.decode(value),true,postresponse.statusCode);
        } else if (postresponse.statusCode == 401) {
          print(value);
          return MenuItemResponse.fromJson(json.decode(value),false,postresponse.statusCode);
        }
        else {
          log("Failed http call."); // Perhaps handle it somehow
          return MenuItemResponse.fromJson(
              json.decode(value), false, postresponse.statusCode);
        }
      });

    } catch (exception) {
      log(exception.toString());
    }

    return null;
  }

  @override
  Future<OrderResponse> postOrderData(String restaurantID,String branchID,String tableID, double grandTotal,
      List<MenuCategoryDishItem> selectedItemsList,
      String uuid,String accessToken) async {
    // final url = "https://www.mocky.io/v2/5b7b131334000038008ed801";
    final url = OrderResponse.endPoint;
    // final httpClient = new HttpClient();

    var body = json.encode({
      'lang': 'en',
      'restaurent_id': restaurantID,
      'branch_id' : branchID,
      'table_id' : tableID,
      'grandtotal' : grandTotal,
      'dish_list' : selectedItemsList
          .map((productItem) => {
        "dish_id": productItem.dishId,
        "dish_name": productItem.dishName,
        "dish_price": productItem.dishPrice,
        "dish_quantity" : productItem.count
      }).toList() ,
      'order_no' :uuid
    });
    try {


      print(body);

      final response = await http.post(Uri.parse(API_SERVER_URL + url),
          body: body,
          headers: {'Content-type': 'application/json','Authorization': 'bearer '+accessToken});

      print("post completed");
      print("status code"+response.statusCode.toString());
      print(response.body);
      if (response.statusCode == 200) {
        print(response.body);
        return OrderResponse.fromJson(json.decode(response.body));
      } else {
        log("Failed http call."); // Perhaps handle it somehow
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }


@override
  Future<MyOrderResponse> getMyOrders(String accessToken,String languageID) async {
    final url = MyOrderResponse.endPoint+"?lang="+languageID;
    try {
      print('accessToken-'+accessToken);
//http://www.mocky.io/v2/5cd196fa330000a38bb12b12
      //API_SERVER_URL + url
      final response =
      await http.get(Uri.parse(API_SERVER_URL + url),
          headers: {'Content-type': 'application/json','Authorization': 'bearer '+accessToken},);
      //print(API_SERVER_URL + url);
      print(response.body);
      print(response.statusCode);

      if (response.statusCode == 200) {
        print(response.body);
        return MyOrderResponse.fromJson(json.decode(response.body),response.statusCode,true);
      } else  if (response.statusCode == 401)
          {
            return MyOrderResponse.fromJsonDecodeError(json.decode(response.body),response.statusCode,false);
          }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }

  @override
  Future<MyOrderResponse> getNextMyOrders(String accessToken,String languageID,String next) async {
    final url = MyOrderResponse.endPoint+"?lang="+languageID;
    try {
      print('accessToken-'+accessToken);
//http://www.mocky.io/v2/5cd196fa330000a38bb12b12
      //API_SERVER_URL + url
      final response =
      await http.get(Uri.parse(next),
        headers: {'Content-type': 'application/json','Authorization': 'bearer '+accessToken},);
      //print(API_SERVER_URL + url);
      print(response.body);
      print(response.statusCode);

      if (response.statusCode == 200) {
        print(response.body);
        return MyOrderResponse.fromJson(json.decode(response.body),response.statusCode,true);
      } else  if (response.statusCode >= 401)
      {
        return MyOrderResponse.fromJsonDecodeError(json.decode(response.body),response.statusCode,false);
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }

  @override
  Future<ForgotPasswordResponse> forgotPassword(String username) async {
    final url = ForgotPasswordResponse.endPoint;
    print("uname-"+username);

  /*  Map<String, String> body = {
      'username': username,
    };
    */
    var body=json.encode({
      'username': username,
    });

    try {

      print("url-"+API_SERVER_URL + url);
      final response = await http.post(Uri.parse(API_SERVER_URL + url),
          body: body, headers: {'Content-type': 'application/x-www-form-urlencoded'});
      print(response.body);
      print('status'+response.statusCode.toString());

      if (response.statusCode == 200) {
        print(response.body);
        return ForgotPasswordResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode >= 400) {
        print(response.body);
        return ForgotPasswordResponse.fromJson(json.decode(response.body));
      }
      else {
        log("Failed http call."); // Perhaps handle it somehow
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }

  @override
  Future<BackgroundImageResponse> getBackgroundImages() async {
    final url = "api/account/getimage";
    try {
      final response = await http.get(Uri.parse(API_SERVER_URL + url));
      print(API_SERVER_URL + url);

      if (response.statusCode == 200) {
        print(response.body);
        return BackgroundImageResponse.fromJson(json.decode(response.body));
      } else {
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }

  @override
  Future<OrderStatusResponse> getStatusList(String accessToken,String languageID) async {
    final url = OrderStatusResponse.endPoint+"?lang="+languageID;
    try {
      print('accessToken-'+accessToken);
      final response =
      await http.get(Uri.parse(API_SERVER_URL + url),
        headers: {'Content-type': 'application/json','Authorization': 'bearer '+accessToken},);
      print(response.body);
      print(response.statusCode);

      if (response.statusCode == 200) {
        print(response.body);
        return OrderStatusResponse.fromJson(json.decode(response.body),response.statusCode,true);
      } else  if (response.statusCode == 401)
      {
        return OrderStatusResponse.fromJsonDecodeError(json.decode(response.body),response.statusCode,false);
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }

  @override
  Future<OrderResponse> postOrderStatus(String restaurantID,String branchID,String orderNo, int orderStatusID,String accessToken) async {
    final url = OrderResponse.endPointStatus;
    // final httpClient = new HttpClient();

    var body = json.encode({
      'lang': 'en',
      'restaurent_id': restaurantID,
      'branch_id' : branchID,
      'order_no' : orderNo,
      'order_status_id' : orderStatusID,
    });
    try {
      print(body);

      final response = await http.post(Uri.parse(API_SERVER_URL + url),
          body: body,
          headers: {'Content-type': 'application/json','Authorization': 'bearer '+accessToken});

      print("post completed");
      print("status code"+response.statusCode.toString());
      print(response.body);
      if (response.statusCode == 200) {
        print(response.body);
        return OrderResponse.fromJson(json.decode(response.body));
      } else {
        log("Failed http call."); // Perhaps handle it somehow
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }
  @override
  Future<OrderResponse> updateItemAvailability(String dishID, int availablity,String accessToken) async {
    final url = OrderResponse.updateAvailability;
    // final httpClient = new HttpClient();

    var body = json.encode({
      'lang': 'en',
      'dish_id' : dishID,
      'dish_Availability' : availablity,
    });
    try {
      print(body);
      print(API_SERVER_URL + url);
      print("accessToken="+accessToken);

      final response = await http.post(Uri.parse(API_SERVER_URL + url),
          body: body,
          headers: {'Content-type': 'application/json','Authorization': 'bearer '+accessToken});

      print("post completed");
      print("status code"+response.statusCode.toString());
      print(response.body);
      if (response.statusCode == 200) {
        print(response.body);
        return OrderResponse.fromJson(json.decode(response.body));
      } else {
        log("Failed http call.");
        return OrderResponse.fromJson(json.decode(response.body));
// Perhaps handle it somehow
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }
/*
  @override
  Future<HomeDataResponse> getHomeFeeds(String slug) async {
    final url = "eventlists/?city="+slug;
    try {
      final response = await http.get(Uri.parse(API_SERVER_URL + url));
      print(API_SERVER_URL + url);

      if (response.statusCode == HttpStatus.OK) {
        print(response.body);
        return HomeDataResponse.fromJson(json.decode(response.body));
      } else {
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }

  @override
  Future<CollaboOrderData> postOrderData(int saloonId,int slotID,String customerName,String customerEmail,String customerPhone,
     double additionalCharges, double grandTotal,List<SlotCategory> selectedItemsList,
      String uuid) async {
    // final url = "https://www.mocky.io/v2/5b7b131334000038008ed801";
    final url = "order/";
    // final httpClient = new HttpClient();

    var body = json.encode({
      'event': saloonId,
      'slot': slotID,
      'cust_name' : customerName,
      'cust_email' : customerEmail,
      'cust_phone' : customerPhone,
      'additioncharges' : additionalCharges,
      'grandtotal' : grandTotal,
      "is_order_seller":false,
      'io_id' : selectedItemsList
          .map((productItem) => {
        "item_id": productItem.item_id,
        "pc_cat": productItem.item_id,
        "item_name": productItem.title,
        "item_price": productItem.price,
        "pc_slot": productItem.seating_id,
        "count" : productItem.count
      }).toList() ,
      'order_no' :uuid
    });
    try {


      print(body);

      final response = await http.post(Uri.parse(API_SERVER_URL + url),
          body: body,
          headers: {'Content-type': 'application/json'});

      print("post completed");
      print("status code"+response.statusCode.toString());
      print(response.body);
      if (response.statusCode == 201) {
        */ /*var json = await response.transform(UTF8.decoder).join();
        var data = JSON.decode(json);*/ /*
        print(response.body);
        return CollaboOrderData.fromJson(json.decode(response.body));
      } else {
        log("Failed http call."); // Perhaps handle it somehow
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }
  @override
  Future<CollaboOrderResponseData> postPaymentConfirmation(String razorpay_orderID,String paymentSignature,String paymentID) async {
    final url = "payment-fetch/";

    var body = json.encode({
      'razorpay_order_id': razorpay_orderID,
      'razorpay_signature' : paymentSignature,
      'razorpay_payment_id' : paymentID,
    });
    try {

      print(body);

      final response = await http.post(Uri.parse(API_SERVER_URL + url),
          body: body,
          headers: {'Content-type': 'application/json'});

      print("post completed");
      print("status code"+response.statusCode.toString());
      print("response="+response.body);
      if (response.statusCode == 200) {
        */ /*var json = await response.transform(UTF8.decoder).join();
                var data = JSON.decode(json);*/ /*
        print(response.body);
        return CollaboOrderResponseData.fromJson(json.decode(response.body));
      } else {
        log("Failed http call."); // Perhaps handle it somehow
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }


 */ /* @override
  Future<SaloonDetailsData> getSaloonDetails(int saloonID) async {
   // final url = "https://www.mocky.io/v2/5b7b131334000038008ed801";
    final url = "api/saloon/"+saloonID.toString()+"/";
    // final httpClient = new HttpClient();

    try {
      */ /**/ /*  var request = await httpClient.getUrl(Uri.parse(API_SERVER_URL + url));
      var response = await request.close();*/ /**/ /*
      final response =
      await http.get(Uri.parse(API_SERVER_URL + url));

      if (response.statusCode == HttpStatus.OK) {
        */ /**/ /*var json = await response.transform(UTF8.decoder).join();
        var data = JSON.decode(json);*/ /**/ /*
        print(response.body);
        return SaloonDetailsData.fromJson(json.decode(response.body));
      } else {
        log("Failed http call."); // Perhaps handle it somehow
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }

  @override
  Future<SaloonOrderData> postOrderData(int saloonId,String customerName,String customerEmail,String customerPhone,
      String dateTime, int additionalCharges, double grandTotal,List<ProductItem> selectedItemsList,
      String uuid) async {
    // final url = "https://www.mocky.io/v2/5b7b131334000038008ed801";
    final url = "api/saloon/order/";
    // final httpClient = new HttpClient();

    var body = json.encode({
      'saloon_id': saloonId,
      'cust_name' : customerName,
      'cust_email' : customerEmail,
      'cust_phone' : customerPhone,
      'bookingtime' : dateTime,
      'additioncharges' : additionalCharges,
      'grandtotal' : grandTotal,
      'io_id' : selectedItemsList
          .map((productItem) => {
        "item_id": productItem.item_id,
        "item_name": productItem.item_name,
        "item_price": productItem.item_price,
        "count" : productItem.count
      }).toList() ,
      'order_no' :uuid
    });
    try {


      print(body);

      final response = await http.post(Uri.parse(API_SERVER_URL + url),
      body: body,
      headers: {'Content-type': 'application/json'});

      print("post completed");
      print("status code"+response.statusCode.toString());
      print(response.body);
   if (response.statusCode == 201) {
        */ /**/ /*var json = await response.transform(UTF8.decoder).join();
        var data = JSON.decode(json);*/ /**/ /*
        print(response.body);
        return SaloonOrderData.fromJson(json.decode(response.body));
      } else {
        log("Failed http call."); // Perhaps handle it somehow
      }
    } catch (exception) {
      log(exception.toString());
    }
    return null;
  }

@override
Future<SaloonOrderResponseData> postPaymentConfirmation(String razorpay_orderID,String paymentSignature,String paymentID) async {
        final url = "api/saloon/payment-fetch/";

        var body = json.encode({
          'razorpay_order_id': razorpay_orderID,
          'razorpay_signature' : paymentSignature,
          'razorpay_payment_id' : paymentID,
        });
        try {

        print(body);

        final response = await http.post(Uri.parse(API_SERVER_URL + url),
        body: body,
        headers: {'Content-type': 'application/json'});

        print("post completed");
        print("status code"+response.statusCode.toString());
        print("response="+response.body);
        if (response.statusCode == 200) {
        */ /**/ /*var json = await response.transform(UTF8.decoder).join();
                var data = JSON.decode(json);*/ /**/ /*
        print(response.body);
        return SaloonOrderResponseData.fromJson(json.decode(response.body));
        } else {
        log("Failed http call."); // Perhaps handle it somehow
        }
        } catch (exception) {
        log(exception.toString());
        }
        return null;
}


*/ /**/ /*  var request = await httpClient.getUrl(Uri.parse(API_SERVER_URL + url));
      var response = await request.close();*/ /**/ /*
*/ /**/ /* final response =
      await http.get(Uri.parse(API_SERVER_URL + url));*/ /**/ /*

*/ /**/ /* http.Response response = await http.post(Uri.encodeFull(API_SERVER_URL + url),
      body: body,
      headers: {'Content-type': 'application/json'});*/ /**/ /*

*/ /**/ /*HttpWithMiddleware httpClient = HttpWithMiddleware.build(middlewares: [
  HttpLogger(logLevel: LogLevel.BODY),
]);*/ /**/ /*
//{"razororder":
// {"id":"order_BqLrYQaWUuEHpi",
// "entity":"order","amount":120000,
// "amount_paid":0,"amount_due":120000,
// "currency":"INR","receipt":"rcptIdrMGrHxmS2zhFVahQR2HqLk",
// "offer_id":null,"status":"created",
// "attempts":0,"notes":{"key":"value"},
// "created_at":1548873033
// },
// "order_details":
// {"saloon_name":3,"cust_name":"Naseem","cust_email":"naseem.10@gmail.com","cust_phone":"919496337251","ordercreatedtime":"2019-01-31T00:00:33.218055Z","bookingtime":"2019-01-31T21:45:38.000000Z","grandtotal":120000.0,"additioncharges":0,"io_id":[{"item_name":"HairCut","count":6,"item_price":200.0}],"order_no":"rcptIdrMGrHxmS2zhFVahQR2HqLk","payment_status":"Payment Pending","order_status":"Order Completed","id":"c3d98132-3305-42f2-a68b-526b6cf8b5f4"}}


*/
}
