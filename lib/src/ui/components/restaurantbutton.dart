import 'package:flutter/material.dart';
import 'package:restaurent_app/src/constants/styles.dart';

class RestaurantButton extends StatelessWidget {

  String titleText;
  VoidCallback onTap;

  RestaurantButton(this.titleText, this.onTap);
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: this.onTap,
        child: Container(width: MediaQuery.of(context).size.width-30,
          height: 50,
          alignment: FractionalOffset.center,
          decoration: BoxDecoration(
            color: Color.fromRGBO(248, 37, 74, 1),
            borderRadius: new BorderRadius.all(Radius.circular(30)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.grey.shade700,
                offset: Offset(1.0, 1.0),
                blurRadius: 1.0,
              ),
            ],
          ),
          child: Text(
            titleText,
            style: buttonTextStyle,
          ),
        ),
      )
    ;
  }
}
