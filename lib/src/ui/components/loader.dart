import 'package:flutter/material.dart';

Widget LoadingWidget(String loadingText,context) {
  return new Container(
    color: Color.fromARGB(200, 0, 0, 0),
      child: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new CircularProgressIndicator(strokeWidth: 4.0),
              new Container(
                padding: const EdgeInsets.all(15.0),
                child: new Text(
                  loadingText,
                  style:
                  new TextStyle(color: Colors.white, fontSize: 18.0,fontWeight:FontWeight.bold),
                ),
              )
            ],
          )));
}