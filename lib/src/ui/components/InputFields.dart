import 'package:flutter/material.dart';
import 'package:restaurent_app/src/constants/colors.dart';

class InputFieldArea extends StatelessWidget {
  final String hint;
  final bool obscure;
  final IconData icon;
  final TextEditingController controller;
  String error;
  Color color;
  Color hintColor;

  InputFieldArea({this.hint, this.obscure, this.icon, this.controller,this.error, this.color,this.hintColor});
  @override
  Widget build(BuildContext context) {
    return (new Container(
      decoration: new BoxDecoration(
        border: new Border(
          bottom: new BorderSide(
            width: 0.5,
            color: colorRestaurant,
          ),
        ),
      ),
      child: new TextFormField(
        obscureText: obscure,
        controller: this.controller
        ,style: TextStyle(
         color:  color,
        ),
        decoration: new InputDecoration(
          icon: new Icon(
            icon,
            color: colorRestaurant ,
          ),
          border: InputBorder.none,
          hintText: hint,
          errorText: error,
          hintStyle:  TextStyle(color: hintColor, fontSize: 16.0 ),
          contentPadding: const EdgeInsets.only(
              top: 30.0, right: 30.0, bottom: 20.0, left: 5.0),
        ),
      ),
    ));
  }
}
