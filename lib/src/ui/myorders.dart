import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:restaurent_app/src/constants/styles.dart';
import 'package:restaurent_app/src/models/auth_utils.dart';
import 'package:restaurent_app/src/models/myorders.dart';
import 'package:restaurent_app/src/network/restaurentapi.dart';
import 'package:restaurent_app/src/restaurentapp.dart';
import 'package:restaurent_app/src/translation_strings.dart';
import 'package:restaurent_app/src/ui/checkout.dart';
import 'package:restaurent_app/src/ui/components/loader.dart';
import 'package:restaurent_app/src/ui/login.dart';
import 'package:restaurent_app/src/ui/myorderdetails.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyOrders extends StatefulWidget {
  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrders> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences _sharedPreferences;
  List<MenuItem> orderList = new List<MenuItem>();
  bool isLoading = false;
  bool nextLoading=false;
  ScrollController _scrollController = new ScrollController();
  String nextUrl="";
  String accessToken;

  String refreshToken;
  String languageID = "";

  @override
  void initState() {
    _fetchAccessToken();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if(nextUrl!=null&&nextUrl!=""&&!isLoading&&!nextLoading)
        _getMoreData();
      }
    });
  }

  void _getMoreData() {
    setState(() {
      nextLoading = true;
      new RestaurantAPIService()
          .getNextMyOrders(accessToken, languageID, nextUrl)
          .then((myOrdersResponse) {
        setState(() {
          if (myOrdersResponse.isSuccess) {
            nextLoading = false;
            orderList.addAll(myOrdersResponse.orderList);
            nextUrl=myOrdersResponse.next;
          }
          else if(!myOrdersResponse.isSuccess &&
              myOrdersResponse.statusCode == 400)
            {
              _showSessionExpiredDialog();
            }
          else if (!myOrdersResponse.isSuccess &&
              myOrdersResponse.statusCode == 401) {
            nextLoading = true;
            new RestaurantAPIService().refresh(refreshToken).then((
                authResponse) {
              AuthUtils.insertDetails(_sharedPreferences, authResponse);
              accessToken = authResponse.accessToken;
              _getMoreData();
            });
          }
        });
      });
    });
  }

  void _showSessionExpiredDialog() {
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return new WillPopScope(
            onWillPop: () async => false,
            child: AlertDialog(
              title: new Text("Session Expired"),
              content:
              new Text(""),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text(Translations.of(context).ok),
                  onPressed: () {

                    Navigator.of(context).pop();
                    AuthUtils.deleteDetails(_sharedPreferences);
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => new LoginPage(),
                      ),
                    );
                  },
                ),
              ],
            ));
      },
    );
  }
  _fetchAccessToken() async {
    _sharedPreferences = await _prefs;
    accessToken = AuthUtils.getToken(_sharedPreferences);
    refreshToken = AuthUtils.getRefreshToken(_sharedPreferences);
    AppModel model = ScopedModel.of(context);

    languageID = model.appLocal.languageCode;

    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,

      appBar: AppBar(
        title: Text(Translations
            .of(context)
            .my_orders, style: titleStyle),
          actions: <Widget>[

            IconButton(
              icon: new Icon(
                  Icons.exit_to_app,
                  color: Colors.white
              ),
              onPressed: () =>
                  showLogOutPopUp(),

            )
          ]
      ),
      body: SafeArea(
        top: false,
        bottom: false,
        child: RefreshIndicator(
          onRefresh: _handleRefresh,
          child: isLoading
              ? Center(child: LoadingWidget(Translations
              .of(context)
              .loading, context))
              : ListView.separated(
              shrinkWrap: true,
              controller: _scrollController,
              itemCount: orderList.length,
              separatorBuilder: (context, index) =>
                  Divider(
                    color: Colors.grey,
                  ),
              itemBuilder: (context, index) {
                var dateFormat = new DateFormat.yMMMMd("en_US");
                return Padding(
                  padding: EdgeInsets.all(15.0),
                  child: GestureDetector(
                    onTap: () =>
                    {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                            new OrderDetails(
                                orderList[index].restaurantId,
                                orderList[index].branchID,
                                orderList[index].orderNO,
                                orderList[index].orderStatusId,
                                orderList[index].memberName,
                                orderList[index].memberPhone,
                                orderList[index].menuCategoryList)
                        )).then((val){
                          if(val)
                            {
                              _fetchAccessToken();
                            }
                    })
                    },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(
                                        border: Border(
                                          left: BorderSide(
                                              color: Colors.grey),
                                          right: BorderSide(
                                              color: Colors.grey),
                                          top: BorderSide(
                                              color: Colors.grey),
                                          bottom: BorderSide(
                                              color: Colors.grey),
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    height: 55,
                                    width: 55,
                                    child: Image.network(
                                      orderList[index]
                                          .restaurantImage,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  SizedBox(width: 10,),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment
                                        .start,
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: <Widget>[
                                      Text(
                                        orderList[index]
                                            .restaurantName,
                                        style: itemTitleStyle
                                        , textAlign: TextAlign.start,
                                      ),
                                      SizedBox(height: 7,),
                                      Text(
                                        orderList[index]
                                            .branchName + ",\n" +
                                            orderList[index].tableName
                                        ,
                                        style: itemPriceStyle
                                        , textAlign: TextAlign.start,
                                      )

                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Text(Translations
                                  .of(context)
                                  .order_no + " : " +
                                  orderList[
                                  index]
                                      .orderNO
                                      .toString(),
                                  style: itemPriceStyle),
                              SizedBox(
                                height: 12.0,
                              ),
                              Text(Translations
                                  .of(context)
                                  .cust_name + " : " +
                                  orderList[
                                  index]
                                      .memberName
                                      .toString(),
                                  style: itemPriceStyle),
                              SizedBox(
                                height: 12.0,
                              ),
                              Text(Translations
                                  .of(context)
                                  .order_placed_on + " : " +
                                  new DateFormat.yMMMMd("en_US").add_jm().format(
                                      orderList[index].orderDate),
                                  style: itemPriceStyle),
                              SizedBox(height: 12.0,),
                              Row(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    flex: 2,
                                    child: Text(Translations
                                        .of(context)
                                        .total_amount + " : "
                                        "SAR " +
                                        orderList[
                                        index]
                                            .grandTotal
                                            .toStringAsFixed(2),
                                        style: itemPriceStyle),
                                  ),

                                ],
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Container(
                                padding: EdgeInsets.all(8.0),
                                decoration: new BoxDecoration(
                                    color: Color.fromRGBO(248, 37, 74, 1)),

                                child: Text(
                                    orderList[index]
                                        .orderStatus,
                                    style: checkoutTitleStyle),

                              ),
                              SizedBox(
                                height: 5.0,
                              ),

                            ],
                          ),
                        ),

                      ],
                    ),
                  ),
                );
              }),
        ),
      ),
    );
  }
  showLogOutPopUp() {

    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(Translations.of(context).log_out),
          content: new Text(Translations.of(context).log_out_confirmation),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(Translations.of(context).cancel),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text(Translations.of(context).ok),
              onPressed: () {
                Navigator.of(context).pop();
                AuthUtils.deleteDetails(_sharedPreferences);
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new LoginPage(),
                  ),
                );
              },
            ),
          ],
        );
      },
    );
  }
  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future<String> _handleRefresh ()async {
    if (accessToken != null) {
      setState(() {
        isLoading = true;
        new RestaurantAPIService()
            .getMyOrders(accessToken, languageID)
            .then((myOrdersResponse) {
          setState(() {
            if (myOrdersResponse.isSuccess) {
              isLoading = false;
              orderList = myOrdersResponse.orderList;
              nextUrl=myOrdersResponse.next;
            }
            else if (!myOrdersResponse.isSuccess &&
                myOrdersResponse.statusCode == 401) {
              isLoading = true;
              new RestaurantAPIService().refresh(refreshToken).then((
                  authResponse) {
                AuthUtils.insertDetails(_sharedPreferences, authResponse);
                accessToken = authResponse.accessToken;
                _fetchAccessToken();
              });
            }
          });
        });
      });
    }
    return 'success';

  }

  void getData() {
    if (accessToken != null) {
      setState(() {
        isLoading = true;
        new RestaurantAPIService()
            .getMyOrders(accessToken, languageID)
            .then((myOrdersResponse) {
          setState(() {
            if (myOrdersResponse.isSuccess) {
              isLoading = false;
              orderList = myOrdersResponse.orderList;
              nextUrl=myOrdersResponse.next;
            }
            else if (!myOrdersResponse.isSuccess &&
                myOrdersResponse.statusCode == 401) {
              isLoading = true;
              new RestaurantAPIService().refresh(refreshToken).then((
                  authResponse) {
                AuthUtils.insertDetails(_sharedPreferences, authResponse);
                accessToken = authResponse.accessToken;
                _fetchAccessToken();
              });
            }
          });
        });
      });
    }

  }
}

