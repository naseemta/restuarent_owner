import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:restaurent_app/src/constants/colors.dart';
import 'package:restaurent_app/src/constants/styles.dart';
import 'package:restaurent_app/src/models/auth_utils.dart';
import 'package:restaurent_app/src/translation_strings.dart';
import 'package:restaurent_app/src/ui/components/restaurantbutton.dart';
import 'package:restaurent_app/src/ui/login.dart';
import 'package:restaurent_app/src/ui/menuhome.dart';
import 'package:restaurent_app/src/ui/myorders.dart';
import 'package:restaurent_app/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:barcode_scan/barcode_scan.dart';

/*
import 'package:barcode_scan/barcode_scan.dart';
*/


class HomePage extends StatefulWidget {
  static final String routeName = 'home';
  //test
  @override
  _HOmePageState createState() => _HOmePageState();
}

class _HOmePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences _sharedPreferences;
  String username="";
  String loginBackground = "";

  final ThemeData _restaurentTheme = ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.teal,
  );

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: colorRestaurant, //top bar color
            statusBarIconBrightness: Brightness.dark, //top bar icons
           //* //bottom bar icons
        )
    );
    return  Scaffold(
        backgroundColor: Colors.white,
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(Translations.of(context).hi+" "+username,style: titleStyle),
          actions: <Widget>[
            GestureDetector(
              child: Padding(
                padding: const EdgeInsets.only(right:15.0),
                child: Center(child: Text(Translations.of(context).my_orders,style:myOrdersStyle)),
              ),
              onTap: ()=>   Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => new MyOrders(),
                ),
              ),
            ),
            IconButton(
              icon: new Icon(
                Icons.exit_to_app,
                color: Colors.white
              ),
              onPressed: () =>
                showLogOutPopUp(),

            )
          ],
        ),
        body: Stack(
          children: <Widget>[
            loginBackground == ""
                ? new Container()
                : Stack(children: <Widget>[CachedNetworkImage(
              imageUrl: loginBackground,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
/*
            placeholder: (context, url) => new CircularProgressIndicator(),
*/
              errorWidget: (context, url, error) => new Icon(Icons.error),
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5)
              ),
            )
            ]),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset('assets/images/scanimage.png',
                    fit: BoxFit.fitWidth,
                    ),
                    SizedBox(height: 30,)
                    ,Text(
                      Translations.of(context).scan_qr_caption,
                      style: buttonCaptionStyle,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        floatingActionButton:  RestaurantButton(Translations.of(context).scan_qr, () {
       setState(() {
         scan();
      /*    new QRCodeReader()
                .setHandlePermissions(true)
                .setExecuteAfterPermissionGranted(true)
                .scan().then((_barcodeString){
                print(_barcodeString);
                  if(!_barcodeString.contains("&")||_barcodeString.split("&").length<3) {
                    Utils.showSnackBar(_scaffoldKey,Translations.of(context).invalid_qr);
                    return;
                  }
               Navigator.push(
                 context,
                 MaterialPageRoute(
                   builder: (context) => new MenuHome(_barcodeString),
                 ),
               );
             });*/
      /*    FlutterQrscaner.startScan().then((value) {
            setState(() {
                print(value);
            });
          });*/
            //_openQRScanner();
          });
        }),
      );

  }
  Future scan() async {
    try {

      String _barcodeString = await BarcodeScanner.scan();
      setState(() {
          print(_barcodeString);
          if(!_barcodeString.contains("&")||_barcodeString.split("&").length<3) {
            Utils.showSnackBar(_scaffoldKey,Translations.of(context).invalid_qr);
            return;
          }
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => new MenuHome(_barcodeString),
            ),
          );
      });

    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
        //  this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
       // setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException{
      //setState(() => this.barcode = 'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
    //  setState(() => this.barcode = 'Unknown error: $e');
    }
}
 /* Future _openQRScanner() async {
    try {
      String _content = await BarcodeScanner.scan();
      setState(() => print( _content));
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        showSnackBar('Please grant camera permission!');
        setState(() {
        //  this._content = null;
        });
      } else {
        showSnackBar('Error: $e');
        setState(() {
        //  this._content = null;
        });
      }
    } on FormatException {
      showSnackBar('User pressed "back" button before scanning');
      setState(() {
        //this._content = null;
      });
    } catch (e) {
      showSnackBar('Error: $e');
      setState(() {
       // this._content = null;
      });
    }

  }*/
  void showSnackBar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () {},
      ),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  void initState() {
    _fetchUsername();
  }

  _fetchUsername() async {
    _sharedPreferences = await _prefs;
    String usernamefromlocal = AuthUtils.getUserName(_sharedPreferences);
    String loginBg = AuthUtils.getHomeBG(_sharedPreferences);
    if (loginBg != null && loginBg != "") {
      setState(() {
        loginBackground=loginBg;
      });
    }
    if (usernamefromlocal != null) {
      setState(() {
        username=usernamefromlocal;
      });
    }
  }

  showLogOutPopUp() {

      // flutter defined function
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text(Translations.of(context).log_out),
            content: new Text(Translations.of(context).log_out_confirmation),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text(Translations.of(context).cancel),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new FlatButton(
                child: new Text(Translations.of(context).ok),
                onPressed: () {
                  Navigator.of(context).pop();
                  AuthUtils.deleteDetails(_sharedPreferences);
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => new LoginPage(),
                    ),
                  );
                },
              ),
            ],
          );
        },
      );
    }
}



