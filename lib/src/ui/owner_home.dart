import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:restaurent_app/src/constants/colors.dart';
import 'package:restaurent_app/src/constants/styles.dart';
import 'package:restaurent_app/src/models/auth_utils.dart';
import 'package:restaurent_app/src/translation_strings.dart';
import 'package:restaurent_app/src/ui/components/restaurantbutton.dart';
import 'package:restaurent_app/src/ui/login.dart';
import 'package:restaurent_app/src/ui/menuhome.dart';
import 'package:restaurent_app/src/ui/menuhomeowner.dart';
import 'package:restaurent_app/src/ui/myorders.dart';
import 'package:restaurent_app/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:barcode_scan/barcode_scan.dart';

/*
import 'package:barcode_scan/barcode_scan.dart';
*/

class OwnerHomePage extends StatefulWidget {
  static final String routeName = 'ownerhome';

  //test
  @override
  _HOmePageState createState() => _HOmePageState();
}

class _HOmePageState extends State<OwnerHomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences _sharedPreferences;
  String username = "";
  String loginBackground = "";
  int selectedIndex=0;
  final ThemeData _restaurentTheme = ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.teal,
  );

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: colorRestaurant, //top bar color
      statusBarIconBrightness: Brightness.dark, //top bar icons
      //* //bottom bar icons
    ));
    final List<Widget> pages = new List<Widget>();
    pages.add(
      MyOrders(),
    );

    pages.add(
      MenuOwnerHome(),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
     /* appBar: AppBar(
        title: Text(selectedIndex==0?Translations
            .of(context)
            .my_orders:Translations
            .of(context)
            .my_orders,
            style: titleStyle),
        actions: <Widget>[
          IconButton(
            icon: new Icon(Icons.exit_to_app, color: Colors.white),
            onPressed: () => showLogOutPopUp(),
          )
        ],
      )*/
      body: pages[selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: selectedIndex,
        onTap: (index) {
          setState(() {
            selectedIndex = index;
          });
        },
        iconSize: 25,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            title: Text(Translations.of(context).orders),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: Text(Translations.of(context).products),
          )
        ],
      ),
    );
  }


  void showSnackBar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () {},
      ),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  void initState() {
    _fetchUsername();
  }

  _fetchUsername() async {
    _sharedPreferences = await _prefs;
    String usernamefromlocal = AuthUtils.getUserName(_sharedPreferences);
    String loginBg = AuthUtils.getHomeBG(_sharedPreferences);
    if (loginBg != null && loginBg != "") {
      setState(() {
        loginBackground = loginBg;
      });
    }
    if (usernamefromlocal != null) {
      setState(() {
        username = usernamefromlocal;
      });
    }
  }

  showLogOutPopUp() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(Translations.of(context).log_out),
          content: new Text(Translations.of(context).log_out_confirmation),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(Translations.of(context).cancel),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text(Translations.of(context).ok),
              onPressed: () {
                Navigator.of(context).pop();
                AuthUtils.deleteDetails(_sharedPreferences);
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new LoginPage(),
                  ),
                );
              },
            ),
          ],
        );
      },
    );
  }
}
