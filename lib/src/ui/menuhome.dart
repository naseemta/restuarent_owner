import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:restaurent_app/src/constants/strings.dart';
import 'package:restaurent_app/src/constants/styles.dart';
import 'package:restaurent_app/src/models/auth_utils.dart';
import 'package:restaurent_app/src/models/menu_model.dart';
import 'package:restaurent_app/src/network/restaurentapi.dart';
import 'package:restaurent_app/src/restaurentapp.dart';
import 'package:restaurent_app/src/translation_strings.dart';
import 'package:restaurent_app/src/ui/checkout.dart';
import 'package:restaurent_app/src/ui/components/loader.dart';
import 'package:restaurent_app/src/ui/myorders.dart';
import 'package:restaurent_app/src/ui/myordersbranch.dart';
import 'package:restaurent_app/src/utils/utils.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MenuHome extends StatefulWidget {
  String barcode;

  MenuHome(this.barcode);

  @override
  _MenuHomeState createState() => _MenuHomeState();
}

class _MenuHomeState extends State<MenuHome>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<MenuCategoryItem> menuCategoryList;
  List<MenuCategoryDishItem> selectedItemsList =
      new List<MenuCategoryDishItem>();

  String API_SERVER_URL = "http://api.unicomerp.net/";
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences _sharedPreferences;
  bool _isLoading = false;
  String accessToken;
  String refreshToken;
  String tableID;
  String restaurantID;
  String branchID;
  String languageID;

  TabController _controller;
  int totalNoOfSeats = 0;
  String menuTitle = "";
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        key: _scaffoldKey,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Color.fromRGBO(113, 117, 122, 1), //change your color here
          ),
          title: Text(
            menuTitle,
            style: TextStyle(
                color: Color.fromRGBO(113, 117, 122, 1), fontSize: 17),
          ),
          actions: <Widget>[
             GestureDetector(
              child: Padding(
                padding: const EdgeInsets.only(right:15.0),
                child: Center(child: Text(Translations.of(context).my_orders, style: TextStyle(
                color: Color.fromRGBO(113, 117, 122, 1), fontSize: 15))),
              ),
              onTap: ()=>   Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => new MyOrdersBranch(branchID),
                ),
              ),
            ),
            GestureDetector(
              onTap: () async {
                if (selectedItemsList.length > 0) {
                  var selectedItemsListTemp = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => new CheckOut(restaurantID, branchID,
                          tableID, selectedItemsList, widget.barcode),
                    ),
                  );
                  if(selectedItemsListTemp!=null)
                  selectedItemsList=selectedItemsListTemp;
                  else
                  selectedItemsList =new List<MenuCategoryDishItem>();
                } else {
                  Utils.showSnackBar(
                      _scaffoldKey, Translations.of(context).add_items);
                  Future.delayed(const Duration(milliseconds: 1500), () {
                    _scaffoldKey.currentState.hideCurrentSnackBar();
                  });
                }
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: Center(
                  child: new Stack(
                    children: <Widget>[
                      new Icon(
                        Icons.shopping_cart,
                        size: 30,
                      ),
                      new Positioned(
                        right: 0,
                        bottom: 12,
                        child: new Container(
                          padding: EdgeInsets.all(1),
                          decoration: new BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          constraints: BoxConstraints(
                            minWidth: 13,
                            minHeight: 13,
                          ),
                          child: new Text(
                            calculateTotal(),
                            style: new TextStyle(
                              color: Colors.white,
                              fontSize: 11,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
          bottom: TabBar(
            controller: _controller,
            isScrollable: true,
            labelColor: Color.fromRGBO(248, 37, 74, 1),
            unselectedLabelColor: Color.fromRGBO(113, 117, 122, 1),
            indicatorColor: Color.fromRGBO(248, 37, 74, 1),
            indicator: UnderlineTabIndicator(
                borderSide: BorderSide(
                    width: 2.0, color: Color.fromRGBO(248, 37, 74, 1))),
            tabs: menuCategoryList.map<Tab>((MenuCategoryItem page) {
              return Tab(
                text: page.menuCategory,
              );
            }).toList(),
          ),
          backgroundColor: Colors.white,
        ),
        body: _isLoading
            ? Center(
                child: LoadingWidget(Translations.of(context).loading, context))
            : TabBarView(
                controller: _controller,
                children: menuCategoryList.map<Widget>((MenuCategoryItem page) {
                  return SafeArea(
                    top: false,
                    bottom: false,
                    child: ListView.separated(
                        shrinkWrap: true,
                        itemCount: page.menuCategoryDishList.length,
                        separatorBuilder: (context, index) => Divider(
                              color: Colors.grey,
                            ),
                        itemBuilder: (context, index) => Padding(
                              padding: EdgeInsets.all(20.0),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 3,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                            page.menuCategoryDishList[index]
                                                .dishName,
                                            style: itemTitleStyle),
                                        SizedBox(
                                          height: 6.0,
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Expanded(
                                              flex: 2,
                                              child: Text(
                                                  page
                                                          .menuCategoryDishList[
                                                              index]
                                                          .dishCurrency +
                                                      " " +
                                                      page
                                                          .menuCategoryDishList[
                                                              index]
                                                          .dishPrice
                                                          .toStringAsFixed(2),
                                                  style: itemPriceStyle),
                                            ),
                                            Expanded(
                                              flex: 2,
                                              child: Text(
                                                  page
                                                          .menuCategoryDishList[
                                                              index]
                                                          .dishCalories
                                                          .toStringAsFixed(2) +
                                                      " calories",
                                                  style: itemPriceStyle),
                                            )
                                          ],
                                        ),
                                        SizedBox(
                                          height: 12.0,
                                        ),
                                        Text(
                                            page.menuCategoryDishList[index]
                                                .dishDescription,
                                            style: itemDescriptionStyle),
                                        SizedBox(
                                          height: 15.0,
                                        ),
                                        Container(
                                          width: 120,
                                          height: 35,
                                          child: NumberSelectWidget(this,
                                              page.menuCategoryDishList[index]),
                                        ),
                                        page.menuCategoryDishList[index]
                                                    .dishAvailability ==
                                                false
                                            ? Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 10, left: 5),
                                                child: Text(
                                                    Translations.of(context)
                                                        .item_not_available,
                                                    style:
                                                        itemDescriptionStyleError))
                                            : new Container(),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          decoration: BoxDecoration(
                                              border: Border(
                                                left: BorderSide(
                                                    color: Colors.grey),
                                                right: BorderSide(
                                                    color: Colors.grey),
                                                top: BorderSide(
                                                    color: Colors.grey),
                                                bottom: BorderSide(
                                                    color: Colors.grey),
                                              ),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          height: 110,
                                          width: 110,
                                          child: CachedNetworkImage(
                                            imageUrl: page
                                                .menuCategoryDishList[index]
                                                .dishImage,
                                            fit: BoxFit.cover,
                                            errorWidget:
                                                (context, url, error) =>
                                                    new Icon(Icons.error),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )),
                  );
                }).toList(),
              ));
  }

  @override
  void initState() {
    super.initState();
    menuCategoryList = new List<MenuCategoryItem>();
    _controller = TabController(vsync: this, length: menuCategoryList.length);
    restaurantID = widget.barcode.split("&")[0].split("=")[1];
    branchID = widget.barcode.split("&")[1].split("=")[1];
    tableID = widget.barcode.split("&")[2].split("=")[1];
    AppModel model = ScopedModel.of(context);
    languageID = model.appLocal.languageCode;
    _fetchAccessToken();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _fetchAccessToken() async {
    _sharedPreferences = await _prefs;
    accessToken = AuthUtils.getToken(_sharedPreferences);
    refreshToken = AuthUtils.getRefreshToken(_sharedPreferences);

    if (accessToken != null) {
      setState(() {
        _showLoading();
        loadMenu();
      });
    }
  }

  Future loadMenu() async {
    final url = MenuItemResponse.endPoint;

    Map<String, String> body = {
      'table_id': tableID,
      'restaurant_id': restaurantID,
      'branch_id': branchID,
      'lang': languageID,
    };
    try {
      print("url-" + API_SERVER_URL + url);

      Map<String, String> headers = {
        'Authorization': 'bearer ' + accessToken,
        'Content-Type': 'multipart/form-data'
      };
      final multipartRequest =
          new http.MultipartRequest('POST', Uri.parse((API_SERVER_URL + url)));
      multipartRequest.headers.addAll(headers);
      multipartRequest.fields.addAll(body);

      StreamedResponse postresponse = await multipartRequest.send();
      postresponse.stream
          .transform(utf8.decoder)
          .transform(json.decoder)
          .listen((value) {
        print(value);
        _hideLoading();
        print('response=' + postresponse.statusCode.toString());
        if (postresponse.statusCode == 200) {
          print(value);
          setState(() {
            menuCategoryList =
                MenuItemResponse.fromJson(value, true, postresponse.statusCode)
                    .restaurantItem
                    .menuCategoryList;
            MenuResponseItem restaurent =
                MenuItemResponse.fromJson(value, true, postresponse.statusCode)
                    .restaurantItem;
            menuTitle =
                restaurent.restaurantName /*+ " - " + restaurent.tableName*/;
            _controller =
                TabController(vsync: this, length: menuCategoryList.length);
          });
        } else if (postresponse.statusCode == 401) {
          print(value);
          _showLoading();
          new RestaurantAPIService().refresh(refreshToken).then((authResponse) {
            AuthUtils.insertDetails(_sharedPreferences, authResponse);
            accessToken = authResponse.accessToken;
            loadMenu();
          });
        } else {
          /*   return MenuItemResponse.fromJson(
              json.decode(value), false, postresponse.statusCode);*/
        }
      });
    } catch (exception) {}
  }

  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  String calculateTotal() {
    int count=0;
    if(selectedItemsList!=null)
    for(int i=0;i<selectedItemsList.length;i++)
      {
        count=count+ selectedItemsList[i].count;  
      }
      return count.toString();
  }
}

class NumberSelectWidget extends StatefulWidget {
  MenuCategoryDishItem slotCategoryList;
  _MenuHomeState parent;

  NumberSelectWidget(this.parent, this.slotCategoryList, {Key key})
      : super(key: key);

  @override
  NumberSelectWidgetState createState() {
    // TODO: implement createState
    return NumberSelectWidgetState();
  }
}

class NumberSelectWidgetState extends State<NumberSelectWidget> {
  int numberOfSeats = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Material(
      clipBehavior: Clip.antiAlias,
      shape: new StadiumBorder(),
      shadowColor: Colors.black,
      elevation: 2.0,
      color: Colors.transparent,
      child: Ink(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              colors: widget.slotCategoryList.dishAvailability == true
                  ? [Color(0xFF4caf50), Color(0xFF4caf50)]
                  : [
                      Color.fromRGBO(113, 117, 122, 1),
                      Color.fromRGBO(113, 117, 122, 1)
                    ]),
        ),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new IconButton(
              icon: new Icon(
                Icons.remove,
                color: Colors.white,
              ),
              onPressed: () => setState(() {
                    if (!widget.slotCategoryList.dishAvailability) {
                      Utils.showSnackBar(widget.parent._scaffoldKey,
                          Translations.of(context).item_not_available);
                      Future.delayed(const Duration(milliseconds: 1500), () {
                        widget.parent._scaffoldKey.currentState
                            .hideCurrentSnackBar();
                      });
                      return;
                    }
                    //   if (this.numberOfSeats > 0) {
                    //   numberOfSeats--;
                      widget.parent.setState(() {
                        if (widget.parent.selectedItemsList != null && widget.parent.selectedItemsList.contains(widget.slotCategoryList)) {

                          if (widget.parent.selectedItemsList[widget .parent.selectedItemsList.indexOf(widget.slotCategoryList)] .count == 0)
                            return;
                          --widget.parent.selectedItemsList[widget.parent.selectedItemsList.indexOf(widget.slotCategoryList)].count;

                          if (widget.parent.selectedItemsList[widget .parent.selectedItemsList .indexOf(widget.slotCategoryList)] .count == 0) {
                            widget.parent.selectedItemsList.removeAt(widget.parent.selectedItemsList.indexOf(widget.slotCategoryList));
                          }
                        }
                      });
                    // }
                  }),
            ),
            new Text(
              widget.slotCategoryList.count.toString(),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color.fromARGB(255, 255, 255, 255),
                fontSize: 16,
                fontFamily: "Helvetica Neue",
              ),
            ),
            new IconButton(
              icon: new Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () => setState(() {
                    if (!widget.slotCategoryList.dishAvailability) {
                      Utils.showSnackBar(widget.parent._scaffoldKey,
                          Translations.of(context).item_not_available);
                      Future.delayed(const Duration(milliseconds: 1500), () {
                        widget.parent._scaffoldKey.currentState
                            .hideCurrentSnackBar();
                      });
                      return;
                    }

                    if (widget.parent.selectedItemsList != null &&  widget.parent.selectedItemsList .contains(widget.slotCategoryList) &&
                        widget.parent.selectedItemsList[widget.parent.selectedItemsList.indexOf(widget.slotCategoryList)].count ==25)
                      return;

                    this.numberOfSeats++;
                    widget.parent.setState(() {
                      widget.parent.totalNoOfSeats++;
                      if (widget.parent.selectedItemsList != null &&
                          widget.parent.selectedItemsList
                              .contains(widget.slotCategoryList)) {
                        ++widget.parent.selectedItemsList[widget.parent.selectedItemsList.indexOf(widget.slotCategoryList)].count;
                      } else {
                        widget.parent.selectedItemsList
                            .add(widget.slotCategoryList);
                        ++widget
                            .parent
                            .selectedItemsList[widget.parent.selectedItemsList
                                .indexOf(widget.slotCategoryList)]
                            .count;
                      }
                    });
                  }),
            )
          ],
        ),
      ),
    );
  }
}
