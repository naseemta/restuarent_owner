import 'package:flutter/material.dart';
import 'package:restaurent_app/src/constants/styles.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NewMember extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<NewMember> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,

      appBar: AppBar(
        title: Text("Member Registration",style: titleStyle),
      ),
      body: WebView(
        initialUrl: 'http://restaurants.unicomerp.net/MemberRegistration.aspx',
       javascriptMode: JavascriptMode.unrestricted,
       /* onWebViewCreated: (WebViewController webViewController) {
          _controller.complete(webViewController);
        },*/
      ),
    );
  }
}
