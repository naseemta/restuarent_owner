import 'package:flutter/material.dart';
import 'package:restaurent_app/src/constants/strings.dart';
import 'package:restaurent_app/src/constants/styles.dart';
import 'package:restaurent_app/src/models/auth_utils.dart';
import 'package:restaurent_app/src/models/menu_model.dart';
import 'package:restaurent_app/src/network/restaurentapi.dart';
import 'package:restaurent_app/src/translation_strings.dart';
import 'package:restaurent_app/src/ui/components/loader.dart';
import 'package:restaurent_app/src/ui/components/restaurantbutton.dart';
import 'package:restaurent_app/src/ui/menuhome.dart';
import 'package:restaurent_app/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

class CheckOut extends StatefulWidget {

  List<MenuCategoryDishItem> selectedItemsList;
  String restaurantID,branchID,tableID,barcode;
  CheckOut(this.restaurantID,this.branchID,this.tableID,this.selectedItemsList,this.barcode);

  @override
  _CheckOutState createState() => _CheckOutState(selectedItemsList);

}

class _CheckOutState extends State<CheckOut> {
  bool isLoading=false;
    List<MenuCategoryDishItem> selectedItemsList;

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences _sharedPreferences;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String accessToken;

  Future<bool> _onWillPop() {

    return new Future.value(true);
  }


  _CheckOutState(this.selectedItemsList);
  @override
  Widget build(BuildContext context) {
    return  new WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.white,
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () => {
              Navigator.pop(context, selectedItemsList)
            },
          ),
          title:  Text(Translations.of(context).order_summary,    style:
          TextStyle(color: Color.fromRGBO(113, 117, 122, 1), fontSize: 17)),
            iconTheme: IconThemeData(
              color:  Color.fromRGBO(113, 117, 122, 1), //change your color here
            ),

        ),
        body:Stack(
                children: <Widget>[
                   CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 10,left: 5,right: 5),
                    child: HomeItemCard(
                        selectedItemsList: widget.selectedItemsList,parentState: this
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
         isLoading ? Center(child: LoadingWidget(Translations.of(context).loading,context)):new Container()
        ]
        ),
        floatingActionButton:  RestaurantButton(Translations.of(context).place_order, () {
            postOrderData();
        }),
      ),
    );
  }
  double calculateTotalPrice() {
    double totalPrice=0.0 ;
    for (int i = 0; i < widget.selectedItemsList.length; i++) {
      totalPrice = totalPrice +
          (this.widget.selectedItemsList[i].dishPrice *
              this.widget.selectedItemsList[i].count);
    }
    return totalPrice;
  }


  @override
  void initState() {
    _fetchAccessToken();
  }

  _fetchAccessToken() async {
    _sharedPreferences = await _prefs;
    accessToken = AuthUtils.getToken(_sharedPreferences);

  }
  void postOrderData() {
    List<Map> itemsList = new List();
    for (int i = 0; i < widget.selectedItemsList.length; i++) {
      MenuCategoryDishItem productItem = new MenuCategoryDishItem();
      productItem.dishId = widget.selectedItemsList[i].dishId;
      productItem.dishName = widget.selectedItemsList[i].dishName;
      productItem.dishPrice = widget.selectedItemsList[i].dishPrice;
      productItem.count = widget.selectedItemsList[i].count;
      itemsList.add(productItem.toJsonData());
      print("before-" + itemsList.length.toString());
    }

    print("after -" + itemsList.length.toString());
    var uuid = new Uuid();

    setState(() {

      isLoading = true;
    });
    new RestaurantAPIService()
        .postOrderData(
        widget.restaurantID,
        widget.branchID,
        widget.tableID,
        calculateTotalPrice(),
        widget.selectedItemsList,
        uuid.v4(),accessToken)
        .then((orderResponseData) {

      this.setState(() {
        isLoading = false;

        if(orderResponseData.isSuccess.contains('true'))
        {
          _showDialog(orderResponseData.orderNo);
        }
        else
        {
          _showDialogError(orderResponseData.message);
        }

        /*isLoading = false;
        saloonDetailsList = saloonDetailsListData;
        popUpSubcategoriesList = saloonDetailsList.saloonDetailsList[0]
            .categoryList[selectedCategroryIndex].subCategoryList;*/
      });
    });
  }
  void _showDialog(String orderNo) {
    // flutter defined function
    showDialog(
      context: context,
       barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return new WillPopScope(
            onWillPop: () async => false,
          child: AlertDialog(
          title: new Text(Translations.of(context).order_placed),
          content: 
              new Text(Translations.of(context).order_placed_caption+"\n\n"+Translations.of(context).order_no+" : "+orderNo),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(Translations.of(context).ok),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();

                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new MenuHome(widget.barcode),
                  ),
                );
              },
            ),
          ],
        ));
      },
    );
  }

  void _showDialogError(String Error) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Error"),
          content: new Text(Error),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(Translations.of(context).ok),
              onPressed: () {
                Navigator.of(context).pop();
                /*Navigator.popUntil(
                  context,
                  ModalRoute.withName('/'),
                );*/
              },
            ),
          ],
        );
      },
    );
  }
}


class HomeItemCard extends StatefulWidget {
  List<MenuCategoryDishItem> selectedItemsList;
  _CheckOutState parentState;
  HomeItemCard({this.selectedItemsList,this.parentState});

  @override
  HomeDataItemState createState() {
    // TODO: implement createState
    return HomeDataItemState();
  }

}

class HomeDataItemState extends State<HomeItemCard> {

  HomeDataItemState();
  Widget createColoumn() {
    List<Widget> list = new List<Widget>();
    for (var i = 0; i < widget.selectedItemsList.length; i++) {
      list.add(buildWidgets(
          widget.selectedItemsList[i].dishName,widget.selectedItemsList[i].dishCurrency,widget.selectedItemsList[i].dishPrice,
          widget.selectedItemsList[i].dishCalories,
          widget.selectedItemsList[i].count,
          widget.selectedItemsList[i].dishPrice*widget.selectedItemsList[i].count, widget.selectedItemsList[i]));
    }
    return new Column(children: list);
  }

  int getNumberOfItems()
  {
    int no=0;
    for (var i = 0; i < widget.selectedItemsList.length; i++) {
      no+=widget.selectedItemsList[i].count;
    }
    return no;
  }

  @override
  Widget build(BuildContext context) {
    Padding _buildTitleSection() {
      return Padding(
        padding: EdgeInsets.only(
              bottom: 15),
        child: Column(
          // Default value for crossAxisAlignment is CrossAxisAlignment.center.
          // We want to align title and description of recipes left:
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Card(
                clipBehavior: Clip.hardEdge,
                elevation: 2,
                child: Container(
                  padding: EdgeInsets.all(15.0),
                  height: 50,
                  width: 350
                  ,decoration: BoxDecoration(
                    color: Color.fromRGBO(248, 37, 74, 1)
                  ),
                  child:  Center(child: Text(widget.selectedItemsList.length.toString()+" "+Translations.of(context).dishes+" - "+getNumberOfItems().toString()+" "+
                  Translations.of(context).items,style: checkoutTitleStyle))
                ) ,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 7,right: 7,bottom: 5.0),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 15.0),
                  createColoumn(),
                  Divider(height: 0.5,color: Colors.grey,),
                  SizedBox(height: 15.0),
                  buildSubWidgets(Translations.of(context).total_amount, calculateTotalPrice(),widget.selectedItemsList.length>0? widget.selectedItemsList[0].dishCurrency:"SAR"),
                
                ],
              ),
            ),
          ],

        ),
      );
    }

    return GestureDetector(
      /*  onTap: () =>
         Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
              new EventFullVIew(homeDataItem: homeDataItem),
            ),
          ),*/
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 0.0),
        child: Column(
          children: <Widget>[
            Card(
              clipBehavior: Clip.hardEdge,
              elevation: 2,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _buildTitleSection(),
                ],
              ),
            ),
            SizedBox(height: 70.0),

          ],
        ),
      ),
    );
  }

  Widget buildWidgets(String category,String currency,double price, double calories, int count, double d, MenuCategoryDishItem selectedItemsListItem) {
    return Column(
      children: <Widget>[
        new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 38
              ,child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 15.0,
                  ),
                  new Container(
                  margin: EdgeInsets.only(left: 5.0, bottom: 5.0),
                  child:  new Text(
                    category, style: itemTitleStyle)
                ),
                  SizedBox(
                    height: 6.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5,bottom: 6),
                    child: Text(
                        currency+" " +
                            price
                                .toStringAsFixed(2),
                        style: itemPriceStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5,bottom: 6),
                    child: Text(
                        calories
                            .toStringAsFixed(2) +
                            " calories",
                        style: itemPriceStyle),
                  )/*,

                  Padding(
                    padding: EdgeInsets.only(left: 5.0, bottom: 15.0),
                    child: Row(
                      crossAxisAlignment:
                      CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Text(
                              currency+" " +
                                  price
                                      .toStringAsFixed(2),
                              style: itemPriceStyle),
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(
                              calories
                                  .toStringAsFixed(2) +
                                  " calories",
                              style: itemPriceStyle),
                        )
                      ],
                    ),
                  )*/,
                ],
              ),
            ),
            Expanded(
              flex: 37,
            /*test*/
              child:
                 Row(
                   mainAxisAlignment: MainAxisAlignment.start,
                   children: <Widget>[
                     new  Container(
                                                width: 113,
                                                height: 35,
                                                child: NumberSelectWidget(widget.parentState,
                                                    selectedItemsListItem),
                      alignment: Alignment(-1.0, 0.0),
                                              ),
                   ],
                 ),

            ),
            Expanded(
              flex: 25
              ,
              child: new Container(
                margin: EdgeInsets.only(right: 5.0, bottom: 10.0),
                child:
                new Text(
                  currency+" " + d .toStringAsFixed(2),
                  style: itemPriceStyle,
                  textAlign: TextAlign.end,
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 10,),
        Divider(height: 0.5,color: Colors.grey,),

      ],
    );
  }
  double calculateTotalPrice() {
    double totalPrice=0.0 ;
    for (int i = 0; i < widget.selectedItemsList.length; i++) {
      totalPrice = totalPrice +
          (this.widget.selectedItemsList[i].dishPrice *
              this.widget.selectedItemsList[i].count);
    }
    return totalPrice;
  }
  Widget buildSubWidgets(String category, double d, String dishCurrency) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex:2
          ,child: new Container(
          margin: EdgeInsets.only(left: 5.0),
          height: 25.0,
          child:  new Text(
            category,
            style: itemTotalStyle,

          ),
        ),
        ),
        Expanded(
          flex: 2
          ,
          child: new Container(
            height: 25.0,
            margin: EdgeInsets.only(right: 5.0, ),
            child:
            new Text(
              dishCurrency+" " + d .toStringAsFixed(2),
              style: itemCheckoutPriceStyle,
              textAlign: TextAlign.end,
            ),
          ),
        )
      ],
    );
  }
}


class NumberSelectWidget extends StatefulWidget {
  MenuCategoryDishItem slotCategoryList;
  _CheckOutState parent;

  NumberSelectWidget(this.parent, this.slotCategoryList, {Key key})
      : super(key: key);

  @override
  NumberSelectWidgetState createState() {
    // TODO: implement createState
    return NumberSelectWidgetState();
  }
}

class NumberSelectWidgetState extends State<NumberSelectWidget> {
  int numberOfSeats = 0;


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Material(
      clipBehavior: Clip.antiAlias,
      shape: new StadiumBorder(),
      shadowColor: Colors.black,
      elevation: 2.0,
      color: Colors.transparent,
      child: Ink(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              colors:  widget.slotCategoryList.dishAvailability==true? [Color(0xFF4caf50), Color(0xFF4caf50)] : [ Color.fromRGBO(113, 117, 122, 1),
              Color.fromRGBO(113, 117, 122, 1)]),
        ),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new IconButton(
              icon: new Icon(
                Icons.remove,
                color: Colors.white,
                size: 14,
              ),
              onPressed: () => setState(() {
                    if(!widget.slotCategoryList.dishAvailability)
                    {
                      Utils.showSnackBar(widget.parent._scaffoldKey, Translations.of(context).item_not_available);
                      Future.delayed(const Duration(milliseconds: 1500), () {
                        widget.parent._scaffoldKey.currentState.hideCurrentSnackBar();

                      });
                          return;
                    }
               
                        widget.parent.setState(() {
                          if (widget.parent.selectedItemsList != null &&
                              widget.parent.selectedItemsList
                                  .contains(widget.slotCategoryList)) {
                            if (widget
                                    .parent
                                    .selectedItemsList[widget
                                        .parent.selectedItemsList
                                        .indexOf(widget.slotCategoryList)]
                                    .count ==
                                0) return;
                            --widget
                                .parent
                                .selectedItemsList[widget
                                    .parent.selectedItemsList
                                    .indexOf(widget.slotCategoryList)]
                                .count;
                            if (widget
                                    .parent
                                    .selectedItemsList[widget
                                        .parent.selectedItemsList
                                        .indexOf(widget.slotCategoryList)]
                                    .count ==
                                0) {
                              widget.parent.selectedItemsList.removeAt(widget
                                  .parent.selectedItemsList
                                  .indexOf(widget.slotCategoryList));
                              if(widget.parent.selectedItemsList.length==0)
                                Navigator.of(context).pop();
                            }
                          }
                        });
                    
                  }),
            ),
            new Text(
              widget.slotCategoryList.count.toString(),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color.fromARGB(255, 255, 255, 255),
                fontSize: 14,
                fontFamily: "Helvetica Neue",
              ),
            ),
            new IconButton(
              icon: new Icon(
                Icons.add,
                color: Colors.white,
               size: 14,

              ),
              onPressed: () => setState(() {
                if(!widget.slotCategoryList.dishAvailability)
                  {
                    Utils.showSnackBar(widget.parent._scaffoldKey, Translations.of(context).item_not_available);
                    Future.delayed(const Duration(milliseconds: 1500), () {
                      widget.parent._scaffoldKey.currentState.hideCurrentSnackBar();

                    });
                    return;
                  }
                      this.numberOfSeats++;
                      if(widget
                              .parent
                              .selectedItemsList[widget.parent.selectedItemsList
                                  .indexOf(widget.slotCategoryList)]
                              .count==25)
                              return;
                      widget.parent.setState(() {
                        if (widget.parent.selectedItemsList != null &&
                            widget.parent.selectedItemsList
                                .contains(widget.slotCategoryList)) {
                          ++widget
                              .parent
                              .selectedItemsList[widget.parent.selectedItemsList
                                  .indexOf(widget.slotCategoryList)]
                              .count;
                        } else {
                          widget.parent.selectedItemsList
                              .add(widget.slotCategoryList);
                          ++widget
                              .parent
                              .selectedItemsList[widget.parent.selectedItemsList
                                  .indexOf(widget.slotCategoryList)]
                              .count;
                        }
                      });
                    
                  }),
            )
          ],
        ),
      ),
    );
  }
}



