import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:restaurent_app/src/constants/colors.dart';
import 'package:restaurent_app/src/constants/strings.dart';
import 'package:restaurent_app/src/constants/styles.dart';
import 'package:restaurent_app/src/models/auth_utils.dart';
import 'package:restaurent_app/src/models/menu_model.dart';
import 'package:restaurent_app/src/network/restaurentapi.dart';
import 'package:restaurent_app/src/restaurentapp.dart';
import 'package:restaurent_app/src/translation_strings.dart';
import 'package:restaurent_app/src/ui/checkout.dart';
import 'package:restaurent_app/src/ui/components/loader.dart';
import 'package:restaurent_app/src/ui/myorders.dart';
import 'package:restaurent_app/src/ui/myordersbranch.dart';
import 'package:restaurent_app/src/utils/utils.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MenuOwnerHome extends StatefulWidget {
 // String barcode;

  MenuOwnerHome();

  @override
  _MenuHomeState createState() => _MenuHomeState();
}

class _MenuHomeState extends State<MenuOwnerHome>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<MenuCategoryItem> menuCategoryList;
  List<MenuCategoryDishItem> selectedItemsList =
      new List<MenuCategoryDishItem>();

  String API_SERVER_URL = "http://api.unicomerp.net/";
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences _sharedPreferences;
  bool _isLoading = false;
  String accessToken;
  String refreshToken;
  String tableID;
  String restaurantID;
  String branchID;
  String languageID;

  TabController _controller;
  int totalNoOfSeats = 0;
  String menuTitle = "";
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
         /* iconTheme: IconThemeData(
            color: Color.fromRGBO(113, 117, 122, 1), //change your color here
          ),*/
          title: Text(
            menuTitle,
            style: TextStyle(
                color:  Colors.white, fontSize: 17),
          ),
          bottom:menuCategoryList!=null? TabBar(
            key: Key(Random().nextDouble().toString()),
            controller: _controller,
            isScrollable: true,
            labelColor: Colors.white/*Color.fromRGBO(248, 37, 74, 1)*/,
            unselectedLabelColor: Color.fromRGBO(189, 189, 189, 1),
            indicatorColor: Colors.white/*Color.fromRGBO(248, 37, 74, 1)*/,
            indicator: UnderlineTabIndicator(
                borderSide: BorderSide(
                    width: 2.0, color:Colors.white /*Color.fromRGBO(248, 37, 74, 1)*/)),
            tabs: menuCategoryList.map<Tab>((MenuCategoryItem page) {
              return Tab(
                text: page.menuCategory,
              );
            }).toList(),
          ): new Container(),
        ),
        body: _isLoading
            ? Center(
                child: LoadingWidget(Translations.of(context).loading, context))
            : menuCategoryList!=null?TabBarView(
                key: Key(Random().nextDouble().toString()),
          controller: _controller,
                children: menuCategoryList.map<Widget>((MenuCategoryItem page) {
                  return SafeArea(
                    top: false,
                    bottom: false,
                    child: RefreshIndicator(
                      onRefresh: _handleRefresh,
                      child: ListView.separated(
                          shrinkWrap: true,
                          itemCount: page.menuCategoryDishList.length,
                          separatorBuilder: (context, index) => Divider(
                                color: Colors.grey,
                              ),
                          itemBuilder: (context, index) => Padding(
                                padding: EdgeInsets.only(left: 15,right: 15,top: 10),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(top: 5,right: 5),
                                      child: Container(
                                        height: 15,
                                        width: 15,
                                        child:page.menuCategoryDishList[index]
                                            .dishType==1?
                                        Image.asset('assets/images/snap_it_non_veg_dish.png',
                                          fit: BoxFit.fitWidth,
                                        ):page.menuCategoryDishList[index]
                                            .dishType==2?
                                        Image.asset('assets/images/snap_it_veg_dish.png',
                                          fit: BoxFit.fitWidth,):
                                        Image.asset('assets/images/egg_dish.png',
                                          fit: BoxFit.fitWidth,),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 8,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                              page.menuCategoryDishList[index]
                                                  .dishName,
                                              style: itemTitleStyle),
                                          SizedBox(
                                            height: 6.0,
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Expanded(
                                                flex: 2,
                                                child: Text(
                                                    page
                                                            .menuCategoryDishList[
                                                                index]
                                                            .dishCurrency +
                                                        " " +
                                                        page
                                                            .menuCategoryDishList[
                                                                index]
                                                            .dishPrice
                                                            .toStringAsFixed(2),
                                                    style: itemPriceStyle),
                                              ),
                                              Expanded(
                                                flex: 2,
                                                child: Padding(
                                                  padding: const EdgeInsets.only(right: 10),
                                                  child: Text(
                                                      page.menuCategoryDishList[index]
                                                          .dishCalories.toInt().toString() + " "+ Translations.of(context).calories,
                                                      textAlign: TextAlign.end,
                                                      style: itemPriceStyle),
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 12.0,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(right: 10),
                                            child: Text(
                                                page.menuCategoryDishList[index]
                                                    .dishDescription,
                                                style: itemDescriptionStyle),
                                          ),
                                          SizedBox(
                                            height: 15.0,
                                          ),

                                       /*   page.menuCategoryDishList[index]
                                                      .dishAvailability ==
                                                  false
                                              ? Padding(
                                                  padding: const EdgeInsets.only(
                                                      top: 10, left: 5),
                                                  child: Text(
                                                      Translations.of(context)
                                                          .item_not_available,
                                                      style:
                                                          itemDescriptionStyleError))
                                              : new Container(),*/
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                            decoration: BoxDecoration(
                                                border: Border(
                                                  left: BorderSide(
                                                      color: Colors.grey),
                                                  right: BorderSide(
                                                      color: Colors.grey),
                                                  top: BorderSide(
                                                      color: Colors.grey),
                                                  bottom: BorderSide(
                                                      color: Colors.grey),
                                                ),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            height: 70,
                                            width: 70,
                                            child: CachedNetworkImage(
                                              imageUrl: page
                                                  .menuCategoryDishList[index]
                                                  .dishImage,
                                              fit: BoxFit.cover,
                                              errorWidget:
                                                  (context, url, error) =>
                                                      new Icon(Icons.error),
                                            ),
                                          ),
                                          Switch(
                                            value: page.menuCategoryDishList[index]
                                                .dishAvailability ,
                                            onChanged: (value) {
                                              setState(() {
                                                print("value changed:"+value.toString());
                                                page.menuCategoryDishList[index]
                                                    .dishAvailability=!page.menuCategoryDishList[index]
                                                    .dishAvailability;
                                                updateAvailablity(page.menuCategoryDishList[index]
                                                    ,value?1:0);
                                              });
                                            },
                                            activeTrackColor: colorRestaurant[500],
                                            activeColor: colorRestaurant,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              )),
                    ),
                  );
                }).toList(),
              ): new Container());
  }

  void updateAvailablity(MenuCategoryDishItem dish,int availablity ) {
    /*setState(() {
      _isLoading = true;
    });*/
    new RestaurantAPIService()
        .updateItemAvailability(
        dish.dishId,
        availablity,
        accessToken)
        .then((orderResponseData) {
      this.setState(() {
        //_isLoading = false;
        if (orderResponseData.isSuccess.contains('true')) {
          _showDialogError(orderResponseData.message);
          //dish.dishAvailability=! dish.dishAvailability;
        } else {
          _showDialogError(orderResponseData.message);
        }

        /*isLoading = false;
        saloonDetailsList = saloonDetailsListData;
        popUpSubcategoriesList = saloonDetailsList.saloonDetailsList[0]
            .categoryList[selectedCategroryIndex].subCategoryList;*/
      });
    });
  }
  void _showDialogError(String Error) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text( Translations.of(context).order_status),
          content: new Text(Error),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(Translations.of(context).ok),
              onPressed: () {
                Navigator.of(context).pop();
                /*Navigator.popUntil(
                  context,
                  ModalRoute.withName('/'),
                );*/
              },
            ),
          ],
        );
      },
    );
  }
  @override
  void initState() {
    super.initState();
    menuCategoryList = new List<MenuCategoryItem>();
    _controller = TabController( vsync: this, length: menuCategoryList.length);
 /*   restaurantID = widget.barcode.split("&")[0].split("=")[1];
    branchID = widget.barcode.split("&")[1].split("=")[1];
    tableID = widget.barcode.split("&")[2].split("=")[1];*/
    AppModel model = ScopedModel.of(context);
    languageID = model.appLocal.languageCode;
    _fetchAccessToken();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _fetchAccessToken() async {
    _sharedPreferences = await _prefs;
    accessToken = AuthUtils.getToken(_sharedPreferences);
    refreshToken = AuthUtils.getRefreshToken(_sharedPreferences);

    if (accessToken != null) {
      setState(() {
        _showLoading();
        loadMenu();
      });
    }
  }

  Future loadMenu() async {
    final url = MenuItemResponse.endPointOwnerMenu;

    Map<String, String> body = {
      'lang': languageID,
    };
    try {
      print("url-" + API_SERVER_URL + url);

      Map<String, String> headers = {
        'Authorization': 'bearer ' + accessToken,
        'Content-Type': 'multipart/form-data'
      };
      final response =
      await http.get(Uri.parse(API_SERVER_URL + url+"?lang="+languageID),
        headers: {'Content-type': 'application/json','Authorization': 'bearer '+accessToken},);

        _hideLoading();
        print('response=' + response.statusCode.toString());
        if (response.statusCode == 200) {
          print(response.body);
          setState(() {

            menuCategoryList =
                MenuItemResponse.fromJson(json.decode(response.body), true, response.statusCode)
                    .restaurantItem
                    .menuCategoryList;
            // _controller=null;
            _controller =
                TabController(vsync: this, length: menuCategoryList.length);
            MenuResponseItem restaurent =
                MenuItemResponse.fromJson(json.decode(response.body), true, response.statusCode)
                    .restaurantItem;
            menuTitle =
                restaurent.restaurantName /*+ " - " + restaurent.tableName*/;

          });
        } else if (response.statusCode == 401) {
          print(response.body);
          _showLoading();
          new RestaurantAPIService().refresh(refreshToken).then((authResponse) {
            AuthUtils.insertDetails(_sharedPreferences, authResponse);
            accessToken = authResponse.accessToken;
            loadMenu();
          });
        } else {
          /*   return MenuItemResponse.fromJson(
              json.decode(value), false, postresponse.statusCode);*/
        }
      }
     catch (exception) {

     }
  }

  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  String calculateTotal() {
    int count=0;
    if(selectedItemsList!=null)
    for(int i=0;i<selectedItemsList.length;i++)
      {
        count=count+ selectedItemsList[i].count;  
      }
      return count.toString();
  }


  Future<void> _handleRefresh() {
    setState(() {
      _showLoading();
      loadMenu();
    });
  }
}

class NumberSelectWidget extends StatefulWidget {
  MenuCategoryDishItem slotCategoryList;
  _MenuHomeState parent;

  NumberSelectWidget(this.parent, this.slotCategoryList, {Key key})
      : super(key: key);

  @override
  NumberSelectWidgetState createState() {
    // TODO: implement createState
    return NumberSelectWidgetState();
  }
}

class NumberSelectWidgetState extends State<NumberSelectWidget> {
  int numberOfSeats = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Material(
      clipBehavior: Clip.antiAlias,
      shape: new StadiumBorder(),
      shadowColor: Colors.black,
      elevation: 2.0,
      color: Colors.transparent,
      child: Ink(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              colors: widget.slotCategoryList.dishAvailability == true
                  ? [Color(0xFF4caf50), Color(0xFF4caf50)]
                  : [
                      Color.fromRGBO(113, 117, 122, 1),
                      Color.fromRGBO(113, 117, 122, 1)
                    ]),
        ),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new IconButton(
              icon: new Icon(
                Icons.remove,
                color: Colors.white,
              ),
              onPressed: () => setState(() {
                    if (!widget.slotCategoryList.dishAvailability) {
                      Utils.showSnackBar(widget.parent._scaffoldKey,
                          Translations.of(context).item_not_available);
                      Future.delayed(const Duration(milliseconds: 1500), () {
                        widget.parent._scaffoldKey.currentState
                            .hideCurrentSnackBar();
                      });
                      return;
                    }
                    //   if (this.numberOfSeats > 0) {
                    //   numberOfSeats--;
                      widget.parent.setState(() {
                        if (widget.parent.selectedItemsList != null && widget.parent.selectedItemsList.contains(widget.slotCategoryList)) {

                          if (widget.parent.selectedItemsList[widget .parent.selectedItemsList.indexOf(widget.slotCategoryList)] .count == 0)
                            return;
                          --widget.parent.selectedItemsList[widget.parent.selectedItemsList.indexOf(widget.slotCategoryList)].count;

                          if (widget.parent.selectedItemsList[widget .parent.selectedItemsList .indexOf(widget.slotCategoryList)] .count == 0) {
                            widget.parent.selectedItemsList.removeAt(widget.parent.selectedItemsList.indexOf(widget.slotCategoryList));
                          }
                        }
                      });
                    // }
                  }),
            ),
            new Text(
              widget.slotCategoryList.count.toString(),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color.fromARGB(255, 255, 255, 255),
                fontSize: 16,
                fontFamily: "Helvetica Neue",
              ),
            ),
            new IconButton(
              icon: new Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () => setState(() {
                    if (!widget.slotCategoryList.dishAvailability) {
                      Utils.showSnackBar(widget.parent._scaffoldKey,
                          Translations.of(context).item_not_available);
                      Future.delayed(const Duration(milliseconds: 1500), () {
                        widget.parent._scaffoldKey.currentState
                            .hideCurrentSnackBar();
                      });
                      return;
                    }

                    if (widget.parent.selectedItemsList != null &&  widget.parent.selectedItemsList .contains(widget.slotCategoryList) &&
                        widget.parent.selectedItemsList[widget.parent.selectedItemsList.indexOf(widget.slotCategoryList)].count ==25)
                      return;

                    this.numberOfSeats++;
                    widget.parent.setState(() {
                      widget.parent.totalNoOfSeats++;
                      if (widget.parent.selectedItemsList != null &&
                          widget.parent.selectedItemsList
                              .contains(widget.slotCategoryList)) {
                        ++widget.parent.selectedItemsList[widget.parent.selectedItemsList.indexOf(widget.slotCategoryList)].count;
                      } else {
                        widget.parent.selectedItemsList
                            .add(widget.slotCategoryList);
                        ++widget
                            .parent
                            .selectedItemsList[widget.parent.selectedItemsList
                                .indexOf(widget.slotCategoryList)]
                            .count;
                      }
                    });
                  }),
            )
          ],
        ),
      ),
    );
  }
}
