import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:restaurent_app/src/constants/styles.dart';
import 'package:restaurent_app/src/models/auth_utils.dart';
import 'package:restaurent_app/src/models/myorders.dart';
import 'package:restaurent_app/src/network/restaurentapi.dart';
import 'package:restaurent_app/src/restaurentapp.dart';
import 'package:restaurent_app/src/translation_strings.dart';
import 'package:restaurent_app/src/ui/checkout.dart';
import 'package:restaurent_app/src/ui/components/loader.dart';
import 'package:restaurent_app/src/ui/myorderdetails.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyOrdersBranch extends StatefulWidget {
  String branchID;
  MyOrdersBranch(this.branchID);

  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrdersBranch> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences _sharedPreferences;
  List<MenuItem> orderList = new List<MenuItem>();

  bool isLoading = false;
  bool nextLoading=false;
  ScrollController _scrollController = new ScrollController();
  String nextUrl="";
  String accessToken;

  String refreshToken;
  String languageID="";

  @override
  void initState() {
    _fetchAccessToken();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if(nextUrl!=null&&nextUrl!=""&&!isLoading&&!nextLoading)
          _getMoreData();
      }
    });
  }
  void _getMoreData() {
    setState(() {
      nextLoading = true;
      new RestaurantAPIService()
          .getNextMyOrders(accessToken, languageID, nextUrl)
          .then((myOrdersResponse) {
        setState(() {
          if (myOrdersResponse.isSuccess) {
            nextLoading = false;
            if(myOrdersResponse.orderList==null)
              return;
            for(int i=0;i<myOrdersResponse.orderList.length;i++ )
            {
              if(myOrdersResponse.orderList[i].branchID!=widget.branchID)
                myOrdersResponse.orderList.removeAt(i);
            }
            orderList.addAll(myOrdersResponse.orderList);
            nextUrl=myOrdersResponse.next;
          }
          else if (!myOrdersResponse.isSuccess &&
              myOrdersResponse.statusCode == 401) {
            nextLoading = true;
            new RestaurantAPIService().refresh(refreshToken).then((
                authResponse) {
              AuthUtils.insertDetails(_sharedPreferences, authResponse);
              accessToken = authResponse.accessToken;
              _getMoreData();
            });
          }
        });
      });
    });
  }
  _fetchAccessToken() async {
    _sharedPreferences = await _prefs;
    accessToken = AuthUtils.getToken(_sharedPreferences);
    refreshToken = AuthUtils.getRefreshToken(_sharedPreferences);
    AppModel model = ScopedModel.of(context);

    languageID=model.appLocal.languageCode;

    if (accessToken != null) {
      setState(() {
        isLoading = true;
        new RestaurantAPIService()
            .getMyOrders(accessToken,languageID)
            .then((myOrdersResponse)
        {
          setState(() {
            if (myOrdersResponse.isSuccess) {
              isLoading = false;
              if(myOrdersResponse.orderList==null)
              return;
              for(int i=0;i<myOrdersResponse.orderList.length;i++ )
              {
                 if(myOrdersResponse.orderList[i].branchID!=widget.branchID)
                 myOrdersResponse.orderList.removeAt(i);
              }
              orderList = myOrdersResponse.orderList;
              nextUrl=myOrdersResponse.next;

            }
            else if (!myOrdersResponse.isSuccess &&
                myOrdersResponse.statusCode == 401) {
              isLoading = true;
              new RestaurantAPIService().refresh(refreshToken).then((
                  authResponse) {
                AuthUtils.insertDetails(_sharedPreferences, authResponse);
                accessToken = authResponse.accessToken;
                _fetchAccessToken();
              });
            }

            });
          });
        });
      }
   }
          @override
          Widget build(BuildContext context)
      {
        return Scaffold(
          backgroundColor: Colors.white,

          appBar: AppBar(
            title: Text(Translations.of(context).my_orders, style: titleStyle),
          ),
          body: SafeArea(
            top: false,
            bottom: false,
            child: isLoading
                ? Center(child: LoadingWidget(Translations.of(context).loading,context))
                : ListView.separated(
                shrinkWrap: true,
                controller: _scrollController,
                itemCount: orderList.length,
                separatorBuilder: (context, index) =>
                    Divider(
                      color: Colors.grey,
                    ),
                itemBuilder: (context, index) {
                  var dateFormat = new DateFormat.yMMMMd("en_US");
                  return Padding(
                      padding: EdgeInsets.all(15.0),
                      child: GestureDetector(
                        onTap: ()=>{
                        Navigator.push(
                        context,
                        MaterialPageRoute(
                        builder: (context) => new OrderDetails(
                        orderList[index].restaurantName,  orderList[index].branchName,
                        orderList[index].tableName,orderList[index].orderStatusId,orderList[index].memberName,orderList[index].memberPhone, orderList[index].menuCategoryList)
                        ))
                        },
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              flex: 3,
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        decoration: BoxDecoration(
                                            border: Border(
                                              left: BorderSide(
                                                  color: Colors.grey),
                                              right: BorderSide(
                                                  color: Colors.grey),
                                              top: BorderSide(
                                                  color: Colors.grey),
                                              bottom: BorderSide(
                                                  color: Colors.grey),
                                            ),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        height: 55,
                                        width: 55,
                                        child: Image.network(
                                          orderList[index]
                                              .restaurantImage,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      SizedBox(width: 10,),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment
                                            .start,
                                        crossAxisAlignment: CrossAxisAlignment
                                            .start,
                                        children: <Widget>[
                                          Text(
                                              orderList[index]
                                                  .restaurantName,
                                              style: itemTitleStyle
                                          ,textAlign: TextAlign.start,
                                          ),
                                          SizedBox(height: 7,),
                                          Text(
                                            orderList[index]
                                                .branchName+",\n"+ orderList[index].tableName
                                            ,
                                            style: itemPriceStyle
                                            ,textAlign: TextAlign.start,
                                          )

                                        ],
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 12.0,
                                  ),
                                  Text(Translations.of(context).order_no+" : "+
                                            
                                                orderList[
                                                index]
                                                    .orderNO
                                                    .toString(),
                                            style: itemPriceStyle),

                                  SizedBox(
                                    height: 12.0,
                                  ),
                               Text(Translations.of(context).order_placed_on+" : "+new DateFormat.yMMMMd("en_US").add_jm().format(orderList[index].orderDate),
                                            style: itemPriceStyle),
                              SizedBox( height: 12.0, ),
                                  Row(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 2,
                                        child: Text(Translations.of(context).total_amount+" : "
                                            "SAR " +
                                                orderList[
                                                index]
                                                    .grandTotal
                                                    .toStringAsFixed(2),
                                            style: itemPriceStyle),
                                      ),

                                    ],
                                  ),
                                  SizedBox(
                                    height: 12.0,
                                  ),
                                 Container(
                                   padding: EdgeInsets.all(8.0),
                                      decoration: new BoxDecoration(
                                          color:Color.fromRGBO(248, 37, 74, 1) ),

                                      child: Text(
                                          orderList[index]
                                              .orderStatus,
                                          style: checkoutTitleStyle),

                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),

                                ],
                              ),
                            ),

                          ],
                        ),
                      ),
                    );
                }),
          ),
        );
      }
  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
    }
