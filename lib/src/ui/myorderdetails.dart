import 'package:flutter/material.dart';
import 'package:restaurent_app/src/constants/colors.dart';
import 'package:restaurent_app/src/constants/strings.dart';
import 'package:restaurent_app/src/constants/styles.dart';
import 'package:restaurent_app/src/models/auth_utils.dart';
import 'package:restaurent_app/src/models/menu_model.dart';
import 'package:restaurent_app/src/models/myorders.dart';
import 'package:restaurent_app/src/models/orderstatus.dart';
import 'package:restaurent_app/src/network/restaurentapi.dart';
import 'package:restaurent_app/src/restaurentapp.dart';
import 'package:restaurent_app/src/translation_strings.dart';
import 'package:restaurent_app/src/ui/components/loader.dart';
import 'package:restaurent_app/src/ui/components/restaurantbutton.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

class OrderDetails extends StatefulWidget {
  List<DishItem> selectedItemsList;
  String restaurantID, branchID, orderNO,membername,memberPhone;
  int orderStatus;
  OrderDetails(
      this.restaurantID, this.branchID, this.orderNO,this.orderStatus,this.membername,this.memberPhone, this.selectedItemsList);

  @override
  _CheckOutState createState() => _CheckOutState();
}

class _CheckOutState extends State<OrderDetails> {
  bool isLoading = false;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences _sharedPreferences;
  List<StatusItem> statusList = new List<StatusItem>();

  String accessToken;
  String refreshToken;
  String languageID = "";
  bool shouldUpdate=false;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () => {
            Navigator.pop(context, shouldUpdate)
            },
          ),
          title: Text(Translations.of(context).order_summary,
              style: TextStyle(
                  color: Color.fromRGBO(113, 117, 122, 1), fontSize: 17)),
          iconTheme: IconThemeData(
            color: Color.fromRGBO(113, 117, 122, 1), //change your color here
          ),
        ),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 10, left: 5, right: 5),
                    child: HomeItemCard(
                        selectedItemsList: widget.selectedItemsList,
                        parentState: this),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  double calculateTotalPrice() {
    double totalPrice = 0.0;
    for (int i = 0; i < widget.selectedItemsList.length; i++) {
      totalPrice = totalPrice +
          (this.widget.selectedItemsList[i].dishPrice *
              this.widget.selectedItemsList[i].count);
    }
    return totalPrice;
  }

  @override
  void initState() {
    _fetchAccessToken();
  }

  _fetchAccessToken() async {
    _sharedPreferences = await _prefs;
    accessToken = AuthUtils.getToken(_sharedPreferences);
    refreshToken = AuthUtils.getRefreshToken(_sharedPreferences);
    AppModel model = ScopedModel.of(context);

    languageID = model.appLocal.languageCode;

    if (accessToken != null) {
      setState(() {
        isLoading = true;
        new RestaurantAPIService()
            .getStatusList(accessToken, languageID)
            .then((myOrdersResponse) {
          setState(() {
            if (myOrdersResponse.isSuccess) {
              isLoading = false;
              statusList = myOrdersResponse.statusList;
            } else if (!myOrdersResponse.isSuccess &&
                myOrdersResponse.statusCode == 401) {
              isLoading = true;
              new RestaurantAPIService()
                  .refresh(refreshToken)
                  .then((authResponse) {
                AuthUtils.insertDetails(_sharedPreferences, authResponse);
                accessToken = authResponse.accessToken;
                _fetchAccessToken();
              });
            }
          });
        });
      });
    }
  }
}

class HomeItemCard extends StatefulWidget {
  List<DishItem> selectedItemsList;
  _CheckOutState parentState;

  HomeItemCard({this.selectedItemsList, this.parentState});

  @override
  HomeDataItemState createState() {
    // TODO: implement createState
    return HomeDataItemState();
  }
}

class HomeDataItemState extends State<HomeItemCard> {
  HomeDataItemState();

  Widget createColoumn() {
    List<Widget> list = new List<Widget>();
    for (var i = 0; i < widget.selectedItemsList.length; i++) {
      list.add(buildWidgets(
          widget.selectedItemsList[i].dishName,
          widget.selectedItemsList[i].dishCurrency,
          widget.selectedItemsList[i].dishPrice,
          widget.selectedItemsList[i].dishCalories,
          widget.selectedItemsList[i].count.toInt(),
          widget.selectedItemsList[i].dishPrice *
              widget.selectedItemsList[i].count,widget.selectedItemsList[i].dishAddOnList,widget.selectedItemsList[i].dishType));
    }
    return new Column(children: list);
  }

  int getNumberOfItems() {
    int no = 0;
    for (var i = 0; i < widget.selectedItemsList.length; i++) {
      no += widget.selectedItemsList[i].count.toInt();
    }
    return no;
  }

  int _currValue = 1;

  @override
  Widget build(BuildContext context) {
   // [{"order_status_id":1,"order_status":"Order Placed"},{"order_status_id":2,"order_status":"Order Prepared"},{"order_status_id":3,"order_status":"Order Delivered"},{"order_status_id":4,"order_status":"Order Closed"}]
   /* if(widget.parentState.widget.orderStatus=="Order Placed"||widget.parentState.widget.orderStatus=="تم الطلب")
      _currValue=1;
    else if(widget.parentState.widget.orderStatus=="Order Prepared"||widget.parentState.widget.orderStatus=="اعد الطلب")
      _currValue=2;
    else if(widget.parentState.widget.orderStatus=="Order Delivered"||widget.parentState.widget.orderStatus=="أجل تسلي")
      _currValue=3;
    else if(widget.parentState.widget.orderStatus=="Order Closed"||widget.parentState.widget.orderStatus=="طلب مغلق")
      _currValue=4;*/

    _currValue=widget.parentState.widget.orderStatus;

    Padding _buildTitleSection() {
      return Padding(
        padding: EdgeInsets.only(bottom: 15),
        child: Column(
          // Default value for crossAxisAlignment is CrossAxisAlignment.center.
          // We want to align title and description of recipes left:
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            Card(
              clipBehavior: Clip.hardEdge,
              elevation: 2,
              child: Container(
                  padding: EdgeInsets.all(15.0),
                  height: 50,
                  width: 350,
                  decoration:
                      BoxDecoration(color: Color.fromRGBO(248, 37, 74, 1)),
                  child: Center(
                      child: Text(
                          widget.selectedItemsList.length.toString() +
                              " " +
                              Translations.of(context).dishes +
                              " - " +
                              getNumberOfItems().toString() +
                              " " +
                              Translations.of(context).items,
                          style: checkoutTitleStyle))),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15.0),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 15.0),
                  createColoumn(),
                  Divider(
                    height: 0.5,
                    color: Colors.grey,
                  ),
                  SizedBox(height: 15.0),
                  buildSubWidgets(
                      Translations.of(context).total_amount,
                      calculateTotalPrice(),
                      widget.selectedItemsList[0].dishCurrency),
                  SizedBox(height: 15.0),
                  widget.parentState.statusList != null
                      ? Card(
                          clipBehavior: Clip.hardEdge,
                          elevation: 2,
                          child: Container(
                              padding: EdgeInsets.all(15.0),
                              height: 50,
                              width: 350,
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(248, 37, 74, 1)),
                              child: Center(
                                  child: Text(
                                      Translations.of(context).order_status,
                                      style: checkoutTitleStyle))),
                        )
                      : new Container(),
                  widget.parentState.statusList != null
                      ? Column(
                          children: widget.parentState.statusList
                              .map((item) => RadioListTile<int>(
                                    title: Text(item.statusName),
                                    activeColor: colorRestaurant,
                                    value: item.statusId,
                                    selected: item.statusId==widget.parentState.widget.orderStatus,
                                    groupValue: _currValue,
                                    onChanged: (int value) {
                                      setState(() {
                                         if(value<=_currValue)
                                          return;

                                         showDialog(
                                           context: context,
                                           builder: (BuildContext context) {
                                             // return object of type Dialog
                                             return AlertDialog(
                                               title: new Text(Translations.of(context).update_status),
                                               content: new Text(Translations.of(context).update_status_confirmation+" "+item.statusName+"?"),
                                               actions: <Widget>[
                                                 // usually buttons at the bottom of the dialog
                                                 new FlatButton(
                                                   child: new Text(Translations.of(context).cancel),
                                                   onPressed: () {
                                                     Navigator.of(context).pop();
                                                   },
                                                 ),
                                                 new FlatButton(
                                                   child: new Text(Translations.of(context).ok),
                                                   onPressed: () {
                                                     Navigator.of(context).pop();
                                                     _currValue = value;
                                                     widget.parentState.shouldUpdate=true;
                                                     widget.parentState.widget.orderStatus=item.statusId;
                                                     postOrderData(_currValue);
                                                   },
                                                 ),
                                               ],
                                             );
                                           },
                                         );

                                      });
                                    },
                                  ))
                              .toList())
                      : new Container()
                ],
              ),
            ),
          ],
        ),
      );
    }

    return GestureDetector(
      /*  onTap: () =>
         Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
              new EventFullVIew(homeDataItem: homeDataItem),
            ),
          ),*/
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
        child: Column(
          children: <Widget>[
            Card(
              clipBehavior: Clip.hardEdge,
              elevation: 2,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top:15.0,left: 10,right: 10,bottom: 5),
                    child: Text(Translations
                        .of(context)
                        .cust_name + " : " +
                        widget.parentState.widget.membername
                            .toString(),
                        style: itemPriceStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(Translations
                        .of(context)
                        .phone_number + " : " +
                        widget.parentState.widget.memberPhone
                            .toString(),
                        style: itemPriceStyle),
                  )
          ,
                  _buildTitleSection(),
                ],
              ),
            ),
            SizedBox(height: 15.0),
          ],
        ),
      ),
    );
  }

  void postOrderData(int orderStatusID ) {
    setState(() {
      widget.parentState.isLoading = true;
    });
    new RestaurantAPIService()
        .postOrderStatus(
            widget.parentState.widget.restaurantID,
            widget.parentState.widget.branchID,
            widget.parentState.widget.orderNO,
            orderStatusID,
        widget.parentState.accessToken)
        .then((orderResponseData) {
      this.setState(() {
        widget.parentState.isLoading = false;
        if (orderResponseData.isSuccess.contains('True')) {
          _showDialogError(orderResponseData.message,true);
        } else {
          _showDialogError(orderResponseData.message,false);
        }

        /*isLoading = false;
        saloonDetailsList = saloonDetailsListData;
        popUpSubcategoriesList = saloonDetailsList.saloonDetailsList[0]
            .categoryList[selectedCategroryIndex].subCategoryList;*/
      });
    });
  }
  void _showDialogError(String Error, bool isSuccess) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text( Translations.of(context).order_status),
          content: new Text(Error),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(Translations.of(context).ok),
              onPressed: () {
                Navigator.of(context).pop();
                if(isSuccess)
                  {
                    Navigator.pop(context, widget.parentState.shouldUpdate);
                  }
                /*Navigator.popUntil(
                  context,
                  ModalRoute.withName('/'),
                );*/
              },
            ),
          ],
        );
      },
    );
  }
  Widget buildWidgets(String category, String currency, double price,
      double calories, int count, double d, List<DishAddonItem> dishAddOnList, int dishType) {
    return Column(
      children: <Widget>[
        new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Container(
                height: 15,
              width: 15,
              child:dishType==1?
              Image.asset('assets/images/snap_it_non_veg_dish.png',
                fit: BoxFit.fitWidth,
              ):dishType==2?
              Image.asset('assets/images/snap_it_veg_dish.png',
                fit: BoxFit.fitWidth,):
              Image.asset('assets/images/egg_dish.png',
                fit: BoxFit.fitWidth,),
              ),
            ),
            Expanded(
              flex: 50,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 15.0,
                  ),
                  new Container(
                      margin: EdgeInsets.only(left: 5.0, bottom: 5.0),
                      child: new Text(category, style: itemTitleStyle)),
                  SizedBox(
                    height: 6.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 5.0, bottom: 15.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Text("SAR " + price.toStringAsFixed(2),
                              style: itemPriceStyle),
                        ),
                        /* Expanded(
                          flex: 2,
                          child: Text(
                              calories
                                  .toInt()
                                  .toString() +
                                  " calories",
                              style: itemPriceStyle),
                        )*/
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: dishAddOnList!=null&&dishAddOnList.length>0?
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: dishAddOnList
                            .map((element) =>
                            Text(element.dishCategoryType==1? "+ " +element.dishAddonName: element.dishCategoryType==2? "- " +element.dishAddonName: element.dishAddonName,
                                style: itemDescriptionStyle)
                        )
                            .toList()):new Container(),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 20,
              child: new Container(
                margin: EdgeInsets.only(left: 15.0,top:15, bottom: 5.0),
                child: new Text(
                  " X " + count.toString(),
                  style: itemPriceStyle,
                ),
              ),
            ),
            Expanded(
              flex: 30,
              child: new Container(
                margin: EdgeInsets.only(right: 5.0,top:15, bottom: 10.0),
                child: new Text(
                  "SAR " + d.toStringAsFixed(2),
                  style: itemPriceStyle,
                  textAlign: TextAlign.end,
                ),
              ),
            )
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Divider(
          height: 0.5,
          color: Colors.grey,
        ),
      ],
    );
  }

  double calculateTotalPrice() {
    double totalPrice = 0.0;
    for (int i = 0; i < widget.selectedItemsList.length; i++) {
      totalPrice = totalPrice +
          (this.widget.selectedItemsList[i].dishPrice *
              this.widget.selectedItemsList[i].count);
    }
    return totalPrice;
  }

  Widget buildSubWidgets(String category, double d, String dishCurrency) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 2,
          child: new Container(
            margin: EdgeInsets.only(left: 5.0),
            height: 25.0,
            child: new Text(
              category,
              style: itemTotalStyle,
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: new Container(
            height: 25.0,
            margin: EdgeInsets.only(
              right: 5.0,
            ),
            child: new Text(
              "SAR " + d.toStringAsFixed(2),
              style: itemCheckoutPriceStyle,
              textAlign: TextAlign.end,
            ),
          ),
        )
      ],
    );
  }
}
