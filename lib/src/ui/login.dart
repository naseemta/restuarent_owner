import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:restaurent_app/src/constants/strings.dart';
import 'package:restaurent_app/src/constants/styles.dart';
import 'package:restaurent_app/src/models/auth_utils.dart';
import 'package:restaurent_app/src/models/backgroundimageresponse.dart';
import 'package:restaurent_app/src/network/restaurentapi.dart';
import 'package:restaurent_app/src/restaurentapp.dart';
import 'package:restaurent_app/src/translation_strings.dart';
import 'package:restaurent_app/src/ui/components/InputFields.dart';
import 'package:restaurent_app/src/ui/components/loader.dart';
import 'package:restaurent_app/src/ui/components/restaurantbutton.dart';
import 'package:restaurent_app/src/ui/forgotpassword.dart';
import 'package:restaurent_app/src/ui/newmember.dart';
import 'package:restaurent_app/src/ui/owner_home.dart';
import 'package:restaurent_app/src/ui/scanhome.dart';
import 'package:restaurent_app/src/utils/utils.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cached_network_image/cached_network_image.dart';

class LoginPage extends StatefulWidget {
  @override
  _loginPageState createState() => _loginPageState();
}

class _loginPageState extends State<LoginPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences _sharedPreferences;
  String loginBackground = "";
  String homebg = "";
  bool _isLoading = false;
  TextEditingController _usernameController, _passwordController;
  String _errorText, _emailError, _passwordError;

  final ThemeData _restaurentTheme = ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.teal,
  );
  @override
  void initState() {
    super.initState();
    new RestaurantAPIService().getBackgroundImages().then((imageresponse) {
      print(imageresponse);
      if (imageresponse != null) {
        // Utils.showSnackBar(_scaffoldKey, 'Something went wrong!');
        setState(() {
          loginBackground = imageresponse.login;
          _saveImage(imageresponse);
        });
      }
    });
    _fetchSessionAndNavigate();
    _usernameController = new TextEditingController();
    _passwordController = new TextEditingController();
  }

  _fetchSessionAndNavigate() async {
    _sharedPreferences = await _prefs;
    String authToken = AuthUtils.getToken(_sharedPreferences);
    String loginBg = AuthUtils.getLoginBG(_sharedPreferences);
    String lang= AuthUtils.getLanguage(_sharedPreferences);
    AppModel model = ScopedModel.of(context);
    if (lang != null && lang != "")
    model.changeDirectionLang(lang);

    if (loginBg != null && loginBg != "") {
      setState(() {
        loginBackground=loginBg;
      });
    }
    if (authToken != null) {
      Navigator.of(_scaffoldKey.currentContext)
          .pushReplacementNamed(OwnerHomePage.routeName);
    }
  }


  _saveImage(BackgroundImageResponse imageresponse) async {
    _sharedPreferences = await _prefs;
    AuthUtils.insertImageDetails(_sharedPreferences, imageresponse);
  }

  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {

    return  Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(/*Translations.of(context).login*/"Snapitt"),
        ),
        body: Stack(children: <Widget>[
          loginBackground == ""
              ? new Container()
              :  Stack(children: <Widget>[/*Image.network(
                  loginBackground,
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.cover,
                )*/
          CachedNetworkImage(
            imageUrl: loginBackground,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
/*
            placeholder: (context, url) => new CircularProgressIndicator(),
*/
            errorWidget: (context, url, error) => new Icon(Icons.error),
          )


          ,
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.5)
                  ),
                )
          ]),
          new SingleChildScrollView(
            padding: const EdgeInsets.all(20.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 120.0,
                ),
                new Form(
                    child: new Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    new InputFieldArea(
                      hint: Translations.of(context).username,
                      obscure: false,
                      icon: Icons.person_outline,
                      controller: _usernameController,
                      error: _emailError,
                      color: Colors.white ,hintColor:  Colors.white,
                    ),
                    new InputFieldArea(
                      hint: Translations.of(context).password,
                      obscure: true,
                      icon: Icons.lock_outline,
                      controller: _passwordController,
                      error: _passwordError, color: Colors.white,hintColor:  Colors.white,
                    ),
                  ],
                )),
                SizedBox(
                  height: 60.0,
                ),
                new RestaurantButton(Translations.of(context).login, () {
                  authenticateUser();
                }),
                SizedBox(
                  height: 60.0,
                ),
               /* GestureDetector(
                  onTap: () => _launchURL(),
                  *//*test*//*
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Text(Translations.of(context).member_registration,
                        style: signUpTitleStyle),
                  ),
                ),*/
               /* GestureDetector(
                  onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => new ForgotPassword(),
                        ),
                      ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(Translations.of(context).forgot_password,
                        style: signUpTitleNormalStyle),
                  ),
                ),*/
              /*  SizedBox(
                  height: 20.0,
                ),*/
                ScopedModelDescendant<AppModel>(
                    builder: (context, child, model) => MaterialButton(
                          onPressed: () {
                            model.changeDirection();
                          },
                          height: 40.0,
                          color: const Color.fromRGBO(248, 37, 74, 1),
                          child: new Text(
                            Translations.of(context).language,
                            style: new TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0.3,
                            ),
                          ),
                        ))
              ],
            ),
          ),
          Center(
              child: _isLoading
                  ? LoadingWidget(Translations.of(context).loading, context)
                  : new Container(height: MediaQuery.of(context).size.height))
        ]),

    );
  }

  authenticateUser() {
    _showLoading();
    if (_valid()) {
      FocusScope.of(context).requestFocus(new FocusNode());
      new RestaurantAPIService()
          .login(_usernameController.text, _passwordController.text)
          .then((authResponse) {
        print(authResponse);
        if (authResponse == null) {
          Utils.showSnackBar(_scaffoldKey, 'Something went wrong!');
        } else if (!authResponse.isSuccess) {
          Utils.showSnackBar(_scaffoldKey, authResponse.errorDescription);
        } else {
          AuthUtils.insertDetails(_sharedPreferences, authResponse);
          Navigator.of(_scaffoldKey.currentContext)
              .pushReplacementNamed(OwnerHomePage.routeName);
        }
        _hideLoading();
      });
    } else {
      setState(() {
        _isLoading = false;
        _emailError;
        _passwordError;
      });
    }
  }

  _launchURL() async {
    const url = 'http://restaurants.unicomerp.net/MemberRegistration.aspx';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _valid() {
    bool valid = true;

    if (_usernameController.text.isEmpty) {
      valid = false;
      _emailError = Translations.of(context).not_valid_username;
    }

    if (_passwordController.text.isEmpty) {
      valid = false;
      _passwordError = Translations.of(context).not_valid_password;
    } else if (_passwordController.text.length < 6) {
      valid = false;
      _passwordError = Translations.of(context).password_is_too_short;
    }

    return valid;
  }
}
