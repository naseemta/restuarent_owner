import 'package:flutter/material.dart';
import 'package:restaurent_app/src/constants/colors.dart';
import 'package:restaurent_app/src/constants/strings.dart';
import 'package:restaurent_app/src/constants/styles.dart';
import 'package:restaurent_app/src/network/restaurentapi.dart';
import 'package:restaurent_app/src/translation_strings.dart';
import 'package:restaurent_app/src/ui/components/InputFields.dart';
import 'package:restaurent_app/src/ui/components/loader.dart';
import 'package:restaurent_app/src/ui/components/restaurantbutton.dart';
import 'package:restaurent_app/src/utils/utils.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  TextEditingController _usernameController;
  String  _emailError;
  bool _isLoading = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _usernameController = new TextEditingController();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,

      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(Translations.of(context).forgot_password,style: titleStyle),
      ),
      body:  Stack(children: <Widget>[
        Padding(
        padding: const EdgeInsets.only(left: 20,right: 20,top: 15),
        child:
          new Column(
          children: <Widget>[
            SizedBox(height: 20,),
            Center(child: Text(Translations.of(context).forgot_password_caption, style:  TextStyle(
              color: colorRestaurant,
              fontWeight: FontWeight.w500,
              fontFamily: "Roboto",
              fontStyle:  FontStyle.normal,
              fontSize: 16.0,
            ),textAlign: TextAlign.center,)),
            SizedBox(height: 20,),
            InputFieldArea(
              hint: Translations.of(context).username,
              obscure: false,
              icon: Icons.person_outline,
              controller: _usernameController,
              error: _emailError,
              color: colorRestaurant,
              hintColor:  colorRestaurant,
            ),
            SizedBox(height: 50,)
            ,
             new RestaurantButton(Translations.of(context).reset_password, () {
               authenticateUser();
             }),
          
          ],
        )
       ,
      ),
       Center(
                child: _isLoading
                    ? LoadingWidget(Translations.of(context).loading, context)
                    : new Container(height: MediaQuery.of(context).size.height))
       ]
        )
      ,
    );
  }
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }
  authenticateUser() {
    _showLoading();
    if (_valid()) {
      FocusScope.of(context).requestFocus(new FocusNode());
      new RestaurantAPIService()
          .forgotPassword(_usernameController.text)
          .then((authResponse) {
        print(authResponse);
        _hideLoading();

        if (authResponse == null) {
          Utils.showSnackBar(_scaffoldKey, 'Something went wrong!');
        } else {

        if(authResponse.status=="1")
          _showDialog( authResponse.message); 
        else
          Utils.showSnackBar(_scaffoldKey, authResponse.message);
        }
      });
    } else {
      setState(() {
        _isLoading = false;
        _emailError;
      });
    }
  }
void _showDialog(String message) {
    // flutter defined function
    showDialog(
      context: context,
       barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return new WillPopScope(
            onWillPop: () async => false,
          child: AlertDialog(
          title: new Text(Translations.of(context).reset_password),
          content: 
              new Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(Translations.of(context).ok),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();

              },
            ),
          ],
        ));
      },
    );
  }
  _valid() {
    bool valid = true;
    if (_usernameController.text.isEmpty) {
      valid = false;
      _emailError = Translations.of(context).not_valid_username;
    }
    return valid;
  }
}
